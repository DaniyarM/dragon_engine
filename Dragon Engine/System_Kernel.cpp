/**************************************************
Dragon Engine 0.1

System_Kernel (.cpp)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/

//-------------------------------------------------
// Includes
//-------------------------------------------------
#include "System_Kernel.h"
//-------------------------------------------------


////////Application////////

mSystem::Application::Application()
{
	hInst    = GetModuleHandle( NULL );
	hWnd     = NULL;
	InitGame = NULL;
    RunGame  = NULL;
	EndGame  = NULL;
}

mSystem::Application::~Application()
{
	UnregisterClass( (LPCSTR)L"DragonWindowClass", hInst );
}

void mSystem::Application::ShowMouse( bool Show )
{
	ShowCursor( Show );
}

HRESULT mSystem::Application::SetWindowCursor( LPCSTR CursorName )
{
	SetCursor( LoadCursorFromFile( CursorName ) );
	
	return S_OK;
}

void mSystem::Application::SetRunGameFunction( Func ExFunc )
{
	RunGame = ExFunc;
}

void mSystem::Application::SetInitGameFunction( Func ExFunc )
{
	InitGame = ExFunc;
}

void mSystem::Application::SetEndGameFunction( Func ExFunc )
{
	EndGame = ExFunc;
}

HRESULT mSystem::Application::Create_Window( char *Name, UINT Width, UINT Height, int flag )
{
	strcpy( WinName, Name );
	width = Width;
	height = Height;
	
	wc.cbSize        = sizeof( WNDCLASSEX );
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInst;
	wc.hIcon         = NULL;

	if( flag == DEFAULT )
	    wc.hCursor   = LoadCursor( NULL, IDC_ARROW );
	else
	    wc.hCursor   = NULL;

	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = (LPCSTR)L"DragonWindowClass";
	wc.hIconSm       = LoadIcon( NULL, IDI_APPLICATION );

	if( !RegisterClassEx( &wc ) )
		return E_FAIL;

	hWnd = CreateWindow( (LPCSTR)L"DragonWindowClass", (LPCSTR)WinName, WS_GAMEWINDOW, (GetSystemMetrics( SM_CXSCREEN ) / 2 - width / 2), 
		                 (GetSystemMetrics( SM_CYSCREEN ) / 2 - height / 2), width, height, NULL, NULL, hInst, NULL );

	if( !hWnd )
	{
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ �������� ����!" );

		return E_FAIL;
	}
	
	return S_OK;
}

HWND mSystem::Application::GetWnd()
{
	return hWnd;
}

HINSTANCE mSystem::Application::GetInst()
{
	return hInst;
}

UINT mSystem::Application::GetWidth()
{
	return width;
}

UINT mSystem::Application::GetHeight()
{
	return height;
}

HRESULT mSystem::Application::Run()
{
	ShowWindow( hWnd, SW_NORMAL );
	UpdateWindow( hWnd );

	if( InitGame == NULL )
	{
        if( SHOW_ERRORS )
	        MsgBox( "������!", "�� ����������� ������� ������������� ����!" );

		return E_FAIL;
	}

	if( (*InitGame)() )
	    MsgBox( "������������� ������", "������ � ������� ������������� ����!" );

	MSG msg = { 0 };

	while( msg.message != WM_QUIT )
	{
		if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else
		{			
			if( RunGame == NULL )
			{
				if( SHOW_ERRORS )
				    MsgBox( "������!", "�� ����������� ������� ����!" );

				return E_FAIL;
			}

		    if( (*RunGame)() )
				MsgBox( "������������� ������", "������ � ������� ���������� ����!" );
		}
	}

	if( EndGame == NULL )
	{
		if( SHOW_ERRORS )
			MsgBox( "������!", "�� ����������� ������� ���������� ����!" );
			
		return E_FAIL;
	}

	if ( (*EndGame)() )
		MsgBox( "������������� ������", "������ � ������� ���������� ����!" );

	return S_OK;
}

void mSystem::Application::End()
{
	PostQuitMessage( 0 );
}

LRESULT CALLBACK mSystem::Application::WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
	case WM_DESTROY:
		PostQuitMessage( 0 );
		break;
	default:
		return DefWindowProc( hWnd, message, wParam, lParam );
	}

	return 0;
}

////////StateManager////////

mSystem::StateManager::StateManager()
{
	pActiveState = NULL;
}

mSystem::StateManager::~StateManager()
{
	DelAll();
}

void mSystem::StateManager::Add( void* Object, void (*mFunc)( void* Obj ) )
{
	if( mFunc == NULL || Object == NULL )
		return;

	State *newActive = new State;

	newActive->Function = mFunc;
	newActive->Obj      = Object;
	newActive->Next     = pActiveState;

	pActiveState = newActive;
}

HRESULT mSystem::StateManager::Process()
{
	if( pActiveState == NULL )
	{
		if( SHOW_ERRORS )
			MsgBox( "������!", "�������� ��������� ����!" );

		return E_FAIL;
	}

	pActiveState->Function( pActiveState->Obj );

	return S_OK;
}

BOOL mSystem::StateManager::Del()
{
	State *buf;
	buf = pActiveState;

	pActiveState = pActiveState->Next;
	buf->Next = NULL;

	delete buf;

	if( pActiveState == NULL )
		return FALSE;

	return TRUE;
}

void mSystem::StateManager::DelAll()
{
	while( Del() );
}

////////ProcessManager////////

mSystem::ProcessManager::ProcessManager()
{
	pFirstProcess = NULL;
}

mSystem::ProcessManager::~ProcessManager()
{
	DelAll();
}

void mSystem::ProcessManager::Add( void* Object, void (*mFunc)( void* Obj ) )
{
	if( mFunc == NULL || Object == NULL )
		return;

	if( pFirstProcess == NULL )
	{
		pFirstProcess = new Process;
		pFirstProcess->Function = mFunc;
		pFirstProcess->Obj      = Object;
		pFirstProcess->Next     = NULL;
		
		return;
	}

	Process *newLast = new Process;

	newLast->Function = mFunc;
	newLast->Obj      = Object;
	newLast->Next     = NULL;

	Process *Last = pFirstProcess;

	while( Last->Next != NULL )
		Last = Last->Next;

	Last->Next = newLast;
}

HRESULT mSystem::ProcessManager::Run()
{
	if( pFirstProcess == NULL )
	{
		if( SHOW_ERRORS )
			MsgBox( "������!", "�������� ��������� ����!" );

		return E_FAIL;
	}

	Process *DoProc = pFirstProcess;
	
	while( DoProc != NULL )
	{
		DoProc->Function( DoProc->Obj );
		DoProc = DoProc->Next;
	}

	return S_OK;
}

BOOL mSystem::ProcessManager::Del()
{
	Process *Last;
	Last = pFirstProcess;

	Process *buf = Last;

	while( Last->Next )
	{
		Last = Last->Next;

		if( Last->Next )
			buf = Last;
	}

	if( buf != Last )
	{
		buf->Next = NULL;
	
		delete Last;
	}
	else
	{
		delete Last;

		return FALSE;
	}

	return TRUE;
}

void mSystem::ProcessManager::DelAll()
{
	while( Del() );
}

void mSystem::ProcessManager::Run( void* Obj )
{
	ProcessManager* Process = (ProcessManager*)Obj;

	if( FAILED( Process->Run() ) )
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ���������� ������ mSystem::ProcessManager!" );
}

////////DataPackage////////

mSystem::DataPackage::DataPackage()
{
	Data = NULL;
	Size = 0;
}

mSystem::DataPackage::~DataPackage()
{
	Release();
}

void* mSystem::DataPackage::Create( UINT size )
{
	Release();

	Size = size;
	Data = (void*)new char[Size];

	return Data;
}

void mSystem::DataPackage::Release()
{
	delete Data;
	Data = NULL;
	Size = 0;
}

BOOL mSystem::DataPackage::Save( char *FileName )
{
	FILE *fp;

	if( Data == NULL || Size )
		return FALSE;

	fp = fopen( FileName, "wb");

	if( fp == NULL )
		return FALSE;

	fwrite( &Size, 1, 4, fp );
	fwrite( Data, 1, Size, fp );

	fclose( fp );

	return TRUE;
}

void* mSystem::DataPackage::Load( char* FileName, UINT *size )
{
	FILE *fp;

	Release();

	fp = fopen( FileName, "rb" );

	if( fp == NULL )
		return NULL;

	fread( &Size, 1, 4, fp );

	Data = (void*)new char[Size];

	if( Data == NULL )
		return NULL;

	fread( Data, 1, Size, fp );

	fclose( fp );

	if( size != NULL )
		*size = Size;

	return Data;
}

void* mSystem::DataPackage::GetData()
{
	return Data;
}

UINT mSystem::DataPackage::GetSize()
{
	return Size;
}

////////Settings////////

mSystem::Settings::Settings( string file )
{
	ifstream cin( file );

	if( !cin )
		if( SHOW_ERRORS )
		    MsgBox( "������!", "�� ��������/�� ������ ���� ��������!" );

	while( !cin.eof() )
	{
		string buf;
		cin >> buf;

		if( buf == "//" )
		{
			getline( cin, buf );

			continue;
		}

		int num;
		cin >> num;

	    Data.insert( pair<string, int> (buf, num) );
	}
}

mSystem::Settings::~Settings()
{
	Data.clear();
}

int mSystem::Settings::GetData( string DataName )
{
	if( Data.find( DataName ) == Data.end() )
		if( SHOW_ERRORS )
			MsgBox( "������ ��� ������ ��������!", "������� �������� �� ������!" );
	
	return Data.find( DataName )->second;
}