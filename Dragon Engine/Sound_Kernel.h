/**************************************************
Dragon Engine 0.1

Sound_Kernel (.h)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/
#ifndef SOUND_KERNEL_H
#define SOUND_KERNEL_H

//-------------------------------------------------
// Debug settings
//-------------------------------------------------
#define SHOW_ERRORS 1
//-------------------------------------------------


//-------------------------------------------------
// Warnings disable
//-------------------------------------------------
#pragma warning ( disable : 4005 )
#pragma warning ( disable : 4018 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4305 )
#pragma warning ( disable : 4715 )
#pragma warning ( disable : 4996 )
#pragma warning ( disable : 4800 )
//#define _CRT_SECURE_NO_WARNINGS
//-------------------------------------------------


//-------------------------------------------------
// Includes
//-------------------------------------------------
#include <xact3.h>
#include <xact3d3.h>
//-------------------------------------------------
#include "Graphic_Kernel.h"
//-------------------------------------------------


//-------------------------------------------------
// Macroses
//-------------------------------------------------
#define MsgBox( tit, mes ) MessageBox( NULL, mes, tit, MB_OK );
//-------------------------------------------------
#define V_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return E_FAIL;                               \
	}                                                \
}
//-------------------------------------------------
#define C_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return 1;                                    \
	}                                                \
}
//-------------------------------------------------
#define SAVE_RELEASE( x )                            \
{                                                    \
	if( x )                                          \
		x->Release();                                \
}                                          
//-------------------------------------------------


//-------------------------------------------------
// General callbacks
//-------------------------------------------------
void CALLBACK XACTNotificationCallback( const XACT_NOTIFICATION* pNotification );
//-------------------------------------------------


namespace Sound
{
	class Music
	{
	public:
		Music( char* GlobalSettings, char* InMemoryWaveBank, char* SoundBank, char* StreamingWaveBank = NULL );
		~Music();

		HRESULT Play();
		HRESULT Pause();
		HRESULT Resume();

		void Add( char* name );

		void SetVolume( float volume );

		static void DoWork( void* Obj );

	protected:
		IXACT3Engine* pEngine;
		IXACT3WaveBank* pInMemoryWaveBank;
		IXACT3SoundBank* pSoundBank;
		
		IXACT3WaveBank* pStreamingWaveBank;

		CRITICAL_SECTION CritSect;

		HRESULT CreateWaveBank( char* file );
		HRESULT CreateStreamingWaveBank( char* file );
		HRESULT CreateSoundBank( char* file );

		XACTINDEX music;

		XACTCATEGORY MusicCategory;
		
		UINT num;
		char* song[1000];

		HRESULT DoWork();
	};
	
	class CueDevice
	{
	public:
		CueDevice( char* GlobalSettings, char* InMemoryWaveBank, char* SoundBank, char* StreamingWaveBank = NULL );
		~CueDevice();

		IXACT3SoundBank* GetSoundBank();
		IXACT3Cue* GetCue();
		IXACT3Engine* GetEngine();

		void SetVolume( float volume );

		static void DoWork( void* Obj );

	protected:
		IXACT3Engine* pEngine;
		IXACT3WaveBank* pInMemoryWaveBank;
		IXACT3SoundBank* pSoundBank;

		IXACT3WaveBank* pStreamingWaveBank;

		XACTCATEGORY CueCategory;

		HRESULT CreateWaveBank( char* file );
		HRESULT CreateStreamingWaveBank( char* file );
		HRESULT CreateSoundBank( char* file );

		HRESULT DoWork();
	};

	class CueDevice3D
	{
	public:
		CueDevice3D( CueDevice* device, Graphic::Camera* Cam, BOOL InverseSound = false );
		~CueDevice3D();

		void SetEmitterPos( float x, float y, float z );
		
		
		IXACT3SoundBank* GetSoundBank();

		X3DAUDIO_DSP_SETTINGS GetDSPSettings();

		static void Update( void* Obj );

	protected:	
		HRESULT Update();

		X3DAUDIO_HANDLE X3DInstance;

		X3DAUDIO_LISTENER Listener;
		X3DAUDIO_EMITTER  Emitter;

		X3DAUDIO_DSP_SETTINGS DSPSettings;

		BOOL mInverseSound;

		XMMATRIX  mView11;

		float MatrixCoefficients[2*8];
		float DelayTimes[2];
		WAVEFORMATEXTENSIBLE wfxFinalMixFormat;
		WORD nDstChannelCount;

		CueDevice* pDevice;
		Graphic::Camera* pCam;
	};
	
	class Cue
	{
	public:
		Cue( CueDevice* device, char* name );
		Cue( CueDevice3D* device, char* name );
		~Cue();

		HRESULT Play();

	protected:
		XACTINDEX cue;
		char* name;

		IXACT3SoundBank* pSoundBank;

		CueDevice* pDevice;
		CueDevice3D* pDevice3D;
	};
};

#endif