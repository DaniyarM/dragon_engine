Texture2D    ObjTexture;
SamplerState ObjSamplerState;

cbuffer ConstantBuffer
{
	float4x4 WVP;
}

struct VS_INPUT
{
	float4 Pos    : POSITION;
	float2 Tex    : TEXCOORD;
	float3 Normal : NORMAL;
};

struct PS_INPUT
{
    float4 Pos   : SV_POSITION;
    float4 Color : COLOR0;
	float2 Tex   : TEXCOORD0;
};

PS_INPUT VS( VS_INPUT input )
{
    //float3 vLightDirection = ( -1, 0, 0.25 );
	//float4 vLightColor     = ( 1, 1, 1, 1 );
	
	PS_INPUT output;

    output.Pos   = mul( input.Pos, WVP );
	//output.Color = saturate( dot( (float3)vLightDirection, input.Pos * 0.5f ) * vLightColor );
	output.Tex   = input.Tex;

    return output;
}

float4 PS( PS_INPUT input ) : SV_Target
{
    return ObjTexture.Sample( ObjSamplerState, input.Tex ) ;//* input.Color;
}

