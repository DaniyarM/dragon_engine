/**************************************************
Dragon Engine 0.1

Main Module (.cpp)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/

//-------------------------------------------------
// Includes
//-------------------------------------------------
#include "main.h"
//-------------------------------------------------


//-------------------------------------------------
// Setup callback functions
//-------------------------------------------------

////////Application functions////////

// This function is call before game execution
bool CALLBACK OnInitGame()
{
	// Set modify cursor
	C_RETURN( Application->SetWindowCursor( "Arrow.cur" ) );

	C_RETURN( Device3D->Create( Settings->GetData( "FullScreen" ) ) );
	Camera->UpdatePerspective();

	C_RETURN( TextShader->Create( "Text.fx" ) );
	C_RETURN( Text->Create( "consolas.png" ) );

	C_RETURN( SpriteShader->Create( "Sprite.fx" ) );
	C_RETURN( Sprite->Create( "Image.jpg" ) );

	C_RETURN( MeshShader->Create( "LoadMesh1.fx" ) );
	C_RETURN( Mesh->Create( "aquafish01.obj", "aquafish01.png" /*"3.obj", "3.png"*/ ) );

	Music->Add( "song1" );
	Music->Add( "song2" );
	Music->Add( "song3" );

	CueDevice3D->SetEmitterPos( 0, 0, 15 );

	ProcessManager->Add( (void*)Device3D, Graphic::Device3D::Frame );
	ProcessManager->Add( (void*)Keybord, Input::Keybord::Update );
	ProcessManager->Add( (void*)Mouse, Input::Mouse::Update );
	ProcessManager->Add( (void*)CueDevice, Sound::CueDevice::DoWork );
	ProcessManager->Add( (void*)Music, Sound::Music::DoWork );
	ProcessManager->Add( (void*)CueDevice3D, Sound::CueDevice3D::Update );

	StateManager->Add( (void*)ProcessManager, mSystem::ProcessManager::Run );

	return 0;
}

// This function shall contain the code of game executed permanently
bool CALLBACK OnRunGame()
{		
	C_RETURN( StateManager->Process() );

	return 0;
}

// This function is call after game execution
bool CALLBACK OnEndGame()
{
	delete Application;
	delete StateManager;
	delete ProcessManager;
	delete Device3D;
	delete Camera;
	delete Text;
	delete TextShader;
	delete Sprite;
	delete SpriteShader;
	delete Mesh;
	delete MeshShader;
	delete InputDevice;
	delete Keybord;
	delete Mouse;
	delete CueDevice;
	delete Cue;
	delete Music;
	delete Settings;
	
	return 0;
}

////////Device functions////////

// This function is call after creating device
/*
bool CALLBACK OnCreateDevice()
{
	return 0;
}

// This function is call after destroing device
bool CALLBACK OnDestroyDevice()
{
	return 0;
}

// This function is call after Resizing Swap Chain
bool CALLBACK OnResizeSwapChain()
{
	return 0;
}
*/

////////Render functions////////

// This function is call before OnFrameRender() function
bool CALLBACK OnFrameMove()
{
	if( Keybord->GetButton( DIK_ESCAPE ) )
		Application->End();
	
	if( Keybord->GetButton( DIK_SPACE ) )
		Cue->Play();

	if( Keybord->GetButton( DIK_P ) )
		Music->Pause();

	if( Keybord->GetButton( DIK_R ) )
		Music->Resume();

	static float v = 1;

	if( Keybord->GetButton( DIK_EQUALS ) )
	{
		v += 0.0002;
		Music->SetVolume( v );
	}
	
	if( Keybord->GetButton( DIK_MINUS ) )
	{
		v -= 0.0002;
		Music->SetVolume( v );
	}

	Music->Play();

	float rotY = Mouse->GetButton( M_X );
	Camera->RotateY( -rotY/3 );
	float rotX = Mouse->GetButton( M_Y );
	Camera->RotateX( -rotX/3 );

	float transSideways = 0;
	if( Keybord->GetButton( DIK_A ) )
		transSideways = 0.4;
	if( Keybord->GetButton( DIK_D ) )
		transSideways = -0.4;

	float transUp = 0;
	if( Keybord->GetButton( DIK_W ) )
		transUp = -0.4;
	if( Keybord->GetButton( DIK_S ) )
		transUp = 0.4;

	Camera->TranslationX( transSideways );

	Camera->TranslationZ( transUp );
	
	return 0;
}

// In this function you may place your render code
bool CALLBACK OnFrameRender()
{
	float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // RGBA
	
	Device3D->ClearAll( ClearColor );

	// Place your code here:

	static double rot = 0;
	rot += 0.01;

	if( rot == 360 )
		rot = 0;
	
	Mesh->Draw( 0, 0, 125, 0, rot, 0, 0.3 );

	//Sprite->Draw( 0, 0, 0.38f, 0.7 );
	//Text->Draw( "������!", 32, XMFLOAT4( 1, 1, 0, 1 ), Device3D->GetWidth()/4*1.5, Device3D->GetHeight()/10 );

	Device3D->Present();
	
	return 0;
}
//-------------------------------------------------


//-------------------------------------------------
// Program start
//-------------------------------------------------
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
	                LPSTR lpCmdLine, int nCmdShow )
{
	////////Set general callbacks////////
	
	// Application callbacks
	Application->SetInitGameFunction( OnInitGame );
	Application->SetRunGameFunction( OnRunGame );
	Application->SetEndGameFunction( OnEndGame );

	// Device callbacks
	/*
	Device3D->SetCreateDeviceFunction( OnCreateDevice );
	Device3D->SetDestroyDeviceFunction( OnDestroyDevice );
	Device3D->SetResizeSwapChainFunction( OnResizeSwapChain );
	*/
	Device3D->SetFrameMoveFunction( OnFrameMove );
	Device3D->SetFrameRenderFunction( OnFrameRender );
	
	// Place your code here:

	V_RETURN( Application->Create_Window( "DragonEngineWindow", Settings->GetData( "Width" )/*800*/, Settings->GetData( "Height" )/*600*/, /*mSystem::Cursor::DEFAULT*/mSystem::Cursor::MODIFY/*(mSystem::Cursor)Settings->GetData( "ModifyCursor" )*/ ) );
	V_RETURN( Application->Run() );

	return S_OK;
}
//-------------------------------------------------



/*

*1) �������� ����� Graphic::Animation
*2) �������� ����� ����� (�����)? ^
*3) �������� ����� ������
*4) �������� ����� ���� (���������, ������� �� ����������� �������)

*����� Object �������� � ������ �������� ( SOL - Standart Objects Library )

*/