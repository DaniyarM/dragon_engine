/**************************************************
Dragon Engine 0.1

Sound_Kernel (.cpp)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/

//-------------------------------------------------
// Includes
//-------------------------------------------------
#include "Sound_Kernel.h"
//-------------------------------------------------


//-------------------------------------------------
// General variables
//-------------------------------------------------
bool MusicStopped = true;
//-------------------------------------------------


//-------------------------------------------------
// General callbacks
//-------------------------------------------------
void CALLBACK XACTNotificationCallback( const XACT_NOTIFICATION* pNotification )
{
	if( pNotification->type == XACTNOTIFICATIONTYPE_CUESTOP )
	{
		MusicStopped = true;
	}
}
//-------------------------------------------------


////////Music////////

Sound::Music::Music( char* GlobalSettings, char* InMemoryWaveBank, char* SoundBank, char* StreamingWaveBank )
{
	pEngine = NULL;
	pInMemoryWaveBank = NULL;
	pSoundBank = NULL;
	pStreamingWaveBank = NULL;

	num = 0;
	ZeroMemory( song, sizeof( song ) );

	InitializeCriticalSection( &CritSect );
	
	CoInitializeEx( NULL, COINIT_MULTITHREADED );
	
	XACT3CreateEngine( /*XACT_FLAG_API_AUDITION_MODE*/0, &pEngine );

	HANDLE pGlobalSettingsData = NULL;
    DWORD dwGlobalSettingsFileSize = 0;

	HANDLE hFile = CreateFile( GlobalSettings, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );

	dwGlobalSettingsFileSize = GetFileSize( hFile, NULL );

	pGlobalSettingsData = CoTaskMemAlloc( dwGlobalSettingsFileSize );

	DWORD dwBytesRead;

	ReadFile( hFile, pGlobalSettingsData, dwGlobalSettingsFileSize, &dwBytesRead, NULL );

	CloseHandle( hFile );

	XACT_RUNTIME_PARAMETERS xrParams = {0};
	xrParams.pGlobalSettingsBuffer = pGlobalSettingsData;
    xrParams.globalSettingsBufferSize = dwGlobalSettingsFileSize;
    xrParams.globalSettingsFlags = XACT_FLAG_GLOBAL_SETTINGS_MANAGEDATA;
	xrParams.lookAheadTime= XACT_ENGINE_LOOKAHEAD_DEFAULT;
	xrParams.fnNotificationCallback = XACTNotificationCallback;
	
	pEngine->Initialize( &xrParams );

	XACT_NOTIFICATION_DESCRIPTION desc = {0};

	// The "wave bank prepared" notification will let the app know when it is save to use
    // play cues that reference streaming wave data.
    desc.flags = XACT_FLAG_NOTIFICATION_PERSIST;
    desc.type = XACTNOTIFICATIONTYPE_WAVEBANKPREPARED;
    pEngine->RegisterNotification( &desc );

	// The "sound bank destroyed" notification will let the app know when it is save to use
    // play cues that reference streaming wave data.
	desc.flags = XACT_FLAG_NOTIFICATION_PERSIST;
    desc.type = XACTNOTIFICATIONTYPE_SOUNDBANKDESTROYED;
    pEngine->RegisterNotification( &desc );

	// The "cue stop" notification will let the app know when it a song stops so a new one 
    // can be played
	desc.flags = XACT_FLAG_NOTIFICATION_PERSIST;
    desc.type = XACTNOTIFICATIONTYPE_CUESTOP;
    desc.cueIndex = XACTINDEX_INVALID;
    pEngine->RegisterNotification( &desc );

	// The "cue prepared" notification will let the app know when it a a cue that uses 
    // streaming data has been prepared so it is ready to be used for zero latency streaming
	desc.flags = XACT_FLAG_NOTIFICATION_PERSIST;
    desc.type = XACTNOTIFICATIONTYPE_CUEPREPARED;
    desc.cueIndex = XACTINDEX_INVALID;
    pEngine->RegisterNotification( &desc );

	CreateWaveBank( InMemoryWaveBank );

	if( StreamingWaveBank != NULL )
		CreateStreamingWaveBank( StreamingWaveBank );

	CreateSoundBank( SoundBank );

	MusicCategory = pEngine->GetCategory( "Music" );

	DoWork();
}

Sound::Music::~Music()
{
	pEngine->ShutDown();
	pEngine->Release();

	//delete pInMemoryWaveBank;
	//delete pStreamingWaveBank;
	//delete pSoundBank;
}

/*void WINAPI Sound::Device::XACTNotificationCallback( const XACT_NOTIFICATION* pNotification )
{
	if( pNotification->type == XACTNOTIFICATIONTYPE_CUESTOP )
    {
        MusicStopped = true;
    }
}*/

HRESULT Sound::Music::CreateWaveBank( char* file )
{
	HANDLE hFile = CreateFile( file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );

	DWORD dwFileSize = GetFileSize( hFile, NULL );

	HANDLE hMapFile = CreateFileMapping( hFile, NULL, PAGE_READONLY, 0, dwFileSize, NULL );

	HANDLE pbInMemoryWaveBank = MapViewOfFile( hMapFile, FILE_MAP_READ, 0, 0, 0 );

	V_RETURN( pEngine->CreateInMemoryWaveBank( pbInMemoryWaveBank, dwFileSize, 0, 0, &pInMemoryWaveBank ) );

	CloseHandle( hMapFile );
	CloseHandle( hFile );

	return S_OK;
}

HRESULT Sound::Music::CreateStreamingWaveBank( char* file )
{
	HANDLE hStreamingWaveBankFile = CreateFile( file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING, NULL );

	XACT_WAVEBANK_STREAMING_PARAMETERS wsParams;
    ZeroMemory( &wsParams, sizeof( XACT_WAVEBANK_STREAMING_PARAMETERS ) );

    wsParams.file = hStreamingWaveBankFile;
    wsParams.offset = 0;
	wsParams.packetSize = 64;

	pEngine->CreateStreamingWaveBank( &wsParams, &pStreamingWaveBank );
	
	return S_OK;
}

HRESULT Sound::Music::CreateSoundBank( char* file )
{
	HANDLE hFile = CreateFile( file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );

	DWORD dwFileSize = GetFileSize( hFile, NULL );

	HANDLE pbSoundBank = new BYTE[dwFileSize];

	DWORD dwBytesRead = 0;

	ReadFile( hFile, pbSoundBank, dwFileSize, &dwBytesRead, NULL );

	V_RETURN( pEngine->CreateSoundBank( pbSoundBank, dwFileSize, 0, 0, &pSoundBank ) );

	CloseHandle( hFile );

	return S_OK;
}

HRESULT Sound::Music::Play()
{
	if( !MusicStopped )
		return S_OK;

	MusicStopped = false;

	static UINT NumberOfMusic = 0;
	
	music = pSoundBank->GetCueIndex( song[NumberOfMusic] );

	NumberOfMusic++;

	if( NumberOfMusic > num-1 )
		NumberOfMusic = 0;

	V_RETURN( pSoundBank->Play( music, 0, 0, NULL ) );
	
	return S_OK;
}

HRESULT Sound::Music::Pause()
{
	pEngine->Pause( MusicCategory, true );
	
	return S_OK;
}

HRESULT Sound::Music::Resume()
{
	pEngine->Pause( MusicCategory, false );

	return S_OK;
}

void Sound::Music::SetVolume( float volume )
{
	if( volume > 1 )
		volume = 1;

	if( volume < 0 )
		volume = 0;

	pEngine->SetVolume( MusicCategory, volume );
}

void Sound::Music::Add( char* name )
{
	song[num++] = name;
}

HRESULT Sound::Music::DoWork()
{
	return pEngine->DoWork();
}

void Sound::Music::DoWork( void* Obj )
{
	Music* mMusic = (Music*)Obj;

	if( FAILED( mMusic->DoWork() ) )
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ���������� ������ Sound::Device!" );
}

////////CueDevice////////

Sound::CueDevice::CueDevice( char* GlobalSettings, char* InMemoryWaveBank, char* SoundBank, char* StreamingWaveBank )
{
	pEngine = NULL;
	pInMemoryWaveBank = NULL;
	pSoundBank = NULL;

	CoInitializeEx( NULL, COINIT_MULTITHREADED );
	
	XACT3CreateEngine( /*XACT_FLAG_API_AUDITION_MODE*/0, &pEngine );

	HANDLE pGlobalSettingsData = NULL;
    DWORD dwGlobalSettingsFileSize = 0;

	HANDLE hFile = CreateFile( GlobalSettings, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );

	dwGlobalSettingsFileSize = GetFileSize( hFile, NULL );

	pGlobalSettingsData = CoTaskMemAlloc( dwGlobalSettingsFileSize );

	DWORD dwBytesRead;

	ReadFile( hFile, pGlobalSettingsData, dwGlobalSettingsFileSize, &dwBytesRead, NULL );

	CloseHandle( hFile );

	XACT_RUNTIME_PARAMETERS xrParams = {0};
	xrParams.pGlobalSettingsBuffer = pGlobalSettingsData;
    xrParams.globalSettingsBufferSize = dwGlobalSettingsFileSize;
    xrParams.globalSettingsFlags = XACT_FLAG_GLOBAL_SETTINGS_MANAGEDATA;
	xrParams.lookAheadTime= XACT_ENGINE_LOOKAHEAD_DEFAULT;

	pEngine->Initialize( &xrParams );

	CreateWaveBank( InMemoryWaveBank );

	if( StreamingWaveBank != NULL )
		CreateStreamingWaveBank( StreamingWaveBank );

	CreateSoundBank( SoundBank );

	CueCategory = pEngine->GetCategory( "Cue" );

	DoWork();
}

Sound::CueDevice::~CueDevice()
{
	pEngine->ShutDown();
	pEngine->Release();

	//delete pInMemoryWaveBank;
	//delete pSoundBank;
}

HRESULT Sound::CueDevice::CreateWaveBank( char* file )
{
	HANDLE hFile = CreateFile( file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );

	DWORD dwFileSize = GetFileSize( hFile, NULL );

	HANDLE hMapFile = CreateFileMapping( hFile, NULL, PAGE_READONLY, 0, dwFileSize, NULL );

	HANDLE pbInMemoryWaveBank = MapViewOfFile( hMapFile, FILE_MAP_READ, 0, 0, 0 );

	V_RETURN( pEngine->CreateInMemoryWaveBank( pbInMemoryWaveBank, dwFileSize, 0, 0, &pInMemoryWaveBank ) );

	CloseHandle( hMapFile );
	CloseHandle( hFile );

	return S_OK;
}

HRESULT Sound::CueDevice::CreateStreamingWaveBank( char* file )
{
	HANDLE hStreamingWaveBankFile = CreateFile( file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING, NULL );

	XACT_WAVEBANK_STREAMING_PARAMETERS wsParams;
    ZeroMemory( &wsParams, sizeof( XACT_WAVEBANK_STREAMING_PARAMETERS ) );

    wsParams.file = hStreamingWaveBankFile;
    wsParams.offset = 0;
	wsParams.packetSize = 64;

	pEngine->CreateStreamingWaveBank( &wsParams, &pStreamingWaveBank );
	
	return S_OK;
}

HRESULT Sound::CueDevice::CreateSoundBank( char* file )
{
	HANDLE hFile = CreateFile( file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );

	DWORD dwFileSize = GetFileSize( hFile, NULL );

	HANDLE pbSoundBank = new BYTE[dwFileSize];

	DWORD dwBytesRead = 0;

	ReadFile( hFile, pbSoundBank, dwFileSize, &dwBytesRead, NULL );

	V_RETURN( pEngine->CreateSoundBank( pbSoundBank, dwFileSize, 0, 0, &pSoundBank ) );

	CloseHandle( hFile );

	return S_OK;
}

IXACT3SoundBank* Sound::CueDevice::GetSoundBank()
{
	return pSoundBank;
}

IXACT3Cue* Sound::CueDevice::GetCue()
{
	return NULL;
}

IXACT3Engine* Sound::CueDevice::GetEngine()
{
	return pEngine;
}

void Sound::CueDevice::SetVolume( float volume )
{
	if( volume > 1 )
		volume = 1;

	if( volume < 0 )
		volume = 0;

	pEngine->SetVolume( CueCategory, volume );
}

HRESULT Sound::CueDevice::DoWork()
{
	return pEngine->DoWork();
}

void Sound::CueDevice::DoWork( void* Obj )
{
	CueDevice* mDevice = (CueDevice*)Obj;

	if( FAILED( mDevice->DoWork() ) )
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ���������� ������ Sound::CueDevice!" );
}

////////CueDevice3D////////

Sound::CueDevice3D::CueDevice3D( CueDevice* device,  Graphic::Camera* Cam, BOOL InverseSound )
{
	pDevice = device;
	pCam = Cam;

	mInverseSound = InverseSound;

	pDevice->GetEngine()->GetFinalMixFormat( &wfxFinalMixFormat );

	nDstChannelCount = wfxFinalMixFormat.Format.nChannels;

	ZeroMemory( &DelayTimes, sizeof( float ) );

	ZeroMemory( &DSPSettings, sizeof( X3DAUDIO_DSP_SETTINGS ) );
    DSPSettings.pMatrixCoefficients = MatrixCoefficients;
    DSPSettings.pDelayTimes = DelayTimes;
    DSPSettings.SrcChannelCount = 2;
    DSPSettings.DstChannelCount = nDstChannelCount;

	XACT3DInitialize( pDevice->GetEngine(), X3DInstance );

	ZeroMemory( &Listener, sizeof( X3DAUDIO_LISTENER ) );

    Listener.OrientFront.x = 0;
	Listener.OrientFront.y = 0;
	Listener.OrientFront.z = 1;
    Listener.OrientTop.x = 0;
	Listener.OrientTop.y = 1;
	Listener.OrientTop.z = 0;
    Listener.Position.x = 0;
	Listener.Position.y = 0;
	Listener.Position.z = 0;
    Listener.Velocity.x = 0;
	Listener.Velocity.y = 0;
	Listener.Velocity.z = 0;

	ZeroMemory( &Emitter, sizeof( X3DAUDIO_EMITTER ) );

    Emitter.pCone = NULL;
    Emitter.OrientFront.x = 0;
	Emitter.OrientFront.y = 0;
	Emitter.OrientFront.z = 1;
    Emitter.OrientTop.x = 0;
	Emitter.OrientTop.y = 1;
	Emitter.OrientTop.z = 0;
    Emitter.Position.x = 0;
	Emitter.Position.y = 0;
	Emitter.Position.z = 0;
    Emitter.Velocity.x = 0;
	Emitter.Velocity.y = 0;
	Emitter.Velocity.z = 0;
    Emitter.ChannelCount = 2;
    Emitter.ChannelRadius = 1.0f;
    Emitter.pChannelAzimuths = NULL;
    Emitter.pVolumeCurve = NULL; //!
    Emitter.pLFECurve = NULL;
    Emitter.pLPFDirectCurve = NULL;
    Emitter.pLPFReverbCurve = NULL;
    Emitter.pReverbCurve = NULL;
    Emitter.CurveDistanceScaler = 1.0f; //!
    Emitter.DopplerScaler = NULL;

	Update();
}

Sound::CueDevice3D::~CueDevice3D()
{
	delete pDevice;
	delete pCam;
}

void Sound::CueDevice3D::SetEmitterPos( float x, float y, float z )
{
	Emitter.Position.x = x;
	Emitter.Position.y = y;
	Emitter.Position.z = z;

	Update();
}

HRESULT Sound::CueDevice3D::Update()
{
	XMMATRIX view11;

	mView11 = pCam->GetView();

	if( !mInverseSound )
		view11 = mView11;
	else
		view11 = XMMatrixInverse( NULL, mView11 );

	//FLOAT3 pos = { view11._11, view11._12, view11._13 };
	//FLOAT3 front = { view11._21, view11._22, view11._23 };
	//FLOAT3 top = { view11._31, view11._32, view11._33 };
		
	Listener.Position.x = view11._11;
	Listener.Position.y = view11._12;
	Listener.Position.z = view11._13;   
	Listener.OrientFront.x = view11._21;
	Listener.OrientFront.y = view11._22;
	Listener.OrientFront.z = view11._23;
	Listener.OrientTop.x = view11._31;
	Listener.OrientTop.y = view11._32;
	Listener.OrientTop.z = view11._33;
	
	//(D3DXVECTOR)&view11._41;
	//(D3DXVECTOR)&view11._21;
	//(D3DXVECTOR)&view11._31;
	
	XACT3DCalculate( X3DInstance, &Listener, &Emitter, &DSPSettings );

	return S_OK;
}
		
IXACT3SoundBank* Sound::CueDevice3D::GetSoundBank()
{
	return pDevice->GetSoundBank();
}

X3DAUDIO_DSP_SETTINGS Sound::CueDevice3D::GetDSPSettings()
{
	return DSPSettings;
}

void Sound::CueDevice3D::Update( void* Obj )
{
	CueDevice3D* mCueDevice3D = (CueDevice3D*)Obj;

	if( FAILED( mCueDevice3D->Update() ) )
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ���������� ������ Sound::CueDevice3D!" );
}

////////Cue////////

Sound::Cue::Cue( Sound::CueDevice* device, char* name )
{
	pDevice3D = NULL;
	pDevice = device;

	pSoundBank = pDevice->GetSoundBank();

	Cue::name = name;
	cue = pSoundBank->GetCueIndex( name );
}

Sound::Cue::Cue( Sound::CueDevice3D* device, char* name )
{
	pDevice = NULL;
	pDevice3D = device;

	pSoundBank = pDevice3D->GetSoundBank();

	Cue::name = name;
	cue = pSoundBank->GetCueIndex( name );
}

Sound::Cue::~Cue()
{
	//delete pDevice;
	//delete pDevice3D;
	//delete pSoundBank;
}

HRESULT Sound::Cue::Play()
{
	static DWORD TimeStart = 0;
	DWORD        TimeCur   = GetTickCount();

	if( TimeStart == 0 )
	{
		TimeStart = TimeCur;
	}
	
	float time = (TimeCur - TimeStart) / 1000.0f;

	static bool played = false;

	if( time >= 0.15 )
	{
		played = false;
		time = 0;
		TimeStart = 0;
	}
	
	if( !played )
	{
		IXACT3Cue* pCue = NULL;

		if( pDevice3D != NULL )
		{
			pSoundBank->Prepare( cue, 0, 0, &pCue );
			X3DAUDIO_DSP_SETTINGS DSPSettings = pDevice3D->GetDSPSettings();
			XACT3DApply( &DSPSettings, pCue );
			
			pCue->Play();

			played = true;

			return S_OK;
		}

		V_RETURN( pSoundBank->Play( cue, 0, 0, NULL ) );
		played = true;
	}

	return S_OK;
}