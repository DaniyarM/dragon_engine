/**************************************************
Dragon Engine 0.1

Main Module (.h)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/

#ifndef MAIN_H
#define MAIN_H

//-------------------------------------------------
// Debug settings
//-------------------------------------------------
#define SHOW_ERRORS 1
//-------------------------------------------------


//-------------------------------------------------
// Macroses
//-------------------------------------------------
#define MsgBox( tit, mes ) MessageBox( NULL, mes, tit, MB_OK );
//-------------------------------------------------
#define V_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return E_FAIL;                               \
	}                                                \
}
//-------------------------------------------------
#define C_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return 1;                                    \
	}                                                \
}
//-------------------------------------------------
#define SAVE_RELEASE( x )                            \
{                                                    \
	if( x )                                          \
		x->Release();                                \
}                                          
//-------------------------------------------------


//-------------------------------------------------
// Warnings disable
//-------------------------------------------------
#pragma warning ( disable : 4005 )
#pragma warning ( disable : 4018 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4305 )
#pragma warning ( disable : 4715 )
#pragma warning ( disable : 4996 )
#pragma warning ( disable : 4800 )
//#define _CRT_SECURE_NO_WARNINGS
//-------------------------------------------------


//-------------------------------------------------
// Windows includes
//-------------------------------------------------
#include <Windows.h>
//-------------------------------------------------


//-------------------------------------------------
// Standart ANSI-C includes
//-------------------------------------------------
//#include <�stdio>
//-------------------------------------------------


//-------------------------------------------------
// Kernels includes
//-------------------------------------------------
#include "System_Kernel.h"
#include "Graphic_Kernel.h"
#include "Input_Kernel.h"
#include "Sound_Kernel.h"
//-------------------------------------------------


//-------------------------------------------------
// Declare copies of classes
//-------------------------------------------------
mSystem::Application*    Application      = new mSystem::Application;
//-------------------------------------------------
mSystem::StateManager*   StateManager     = new mSystem::StateManager;
//-------------------------------------------------
mSystem::ProcessManager* ProcessManager   = new mSystem::ProcessManager;
//-------------------------------------------------
mSystem::Settings*       Settings         = new mSystem::Settings( "Settings.txt" );
//-------------------------------------------------
Graphic::Device3D*       Device3D         = new Graphic::Device3D( Application );
//-------------------------------------------------
Graphic::Camera*         Camera           = new Graphic::Camera( Device3D );
//-------------------------------------------------
Graphic::Shader*         TextShader       = new Graphic::Shader( Device3D );
//-------------------------------------------------
Graphic::Text*           Text             = new Graphic::Text( Device3D, TextShader ); 
//-------------------------------------------------
Graphic::Shader*         SpriteShader     = new Graphic::Shader( Device3D );
//-------------------------------------------------
Graphic::Sprite*         Sprite           = new Graphic::Sprite( Device3D, SpriteShader );
//-------------------------------------------------
Graphic::Shader*         MeshShader       = new Graphic::Shader( Device3D );
//-------------------------------------------------
Graphic::Mesh*           Mesh             = new Graphic::Mesh( Device3D, MeshShader, Camera );
//-------------------------------------------------
Input::Device*           InputDevice      = new Input::Device( Application );
//-------------------------------------------------
Input::Keybord*          Keybord          = new Input::Keybord( InputDevice );
//-------------------------------------------------
Input::Mouse*            Mouse            = new Input::Mouse( InputDevice );
//-------------------------------------------------
Sound::CueDevice*        CueDevice        = new Sound::CueDevice( "Settings2.xgs", "InMemoryWaveBank2.xwb", "Sounds2.xsb"/*, "StreamingWaveBank.xwb"*/ );
//-------------------------------------------------
Sound::CueDevice3D*      CueDevice3D      = new Sound::CueDevice3D( CueDevice, Camera, Settings->GetData( "Sound3DInverse" ) );
//-------------------------------------------------
Sound::Cue*              Cue              = new Sound::Cue( CueDevice3D, "zap" );
//-------------------------------------------------
Sound::Music*            Music            = new Sound::Music( "Settings2.xgs", "InMemoryWaveBank2.xwb", "Sounds2.xsb"/*, "StreamingWaveBank.xwb"*/ );
//-------------------------------------------------


//-------------------------------------------------
// Global variables
//-------------------------------------------------
/*Empty*/
//-------------------------------------------------


#endif