/**************************************************
Dragon Engine 0.1

System_Kernel (.h)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/
#ifndef SYSTEM_KERNEL_H
#define SYSTEM_KERNEL_H

//-------------------------------------------------
// Debug settings
//-------------------------------------------------
#define SHOW_ERRORS 1
//-------------------------------------------------


//-------------------------------------------------
// Warnings disable
//-------------------------------------------------
#pragma warning ( disable : 4005 )
#pragma warning ( disable : 4018 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4305 )
#pragma warning ( disable : 4715 )
#pragma warning ( disable : 4996 )
#pragma warning ( disable : 4800 )
//#define _CRT_SECURE_NO_WARNINGS
//-------------------------------------------------


//-------------------------------------------------
// Includes
//-------------------------------------------------
#include <Windows.h>
//-------------------------------------------------
#include <stdio.h>
#include <map>
#include <string>
#include <fstream>
//-------------------------------------------------


//-------------------------------------------------
// Namespaces
//-------------------------------------------------
using namespace std;
//-------------------------------------------------


//-------------------------------------------------
// Macroses
//-------------------------------------------------
#define MsgBox( tit, mes ) MessageBox( NULL, mes, tit, MB_OK );
//-------------------------------------------------
#define V_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return E_FAIL;                               \
	}                                                \
}
//-------------------------------------------------
#define C_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return 1;                                    \
	}                                                \
}
//-------------------------------------------------
#define SAVE_RELEASE( x )                            \
{                                                    \
	if( x )                                          \
		x->Release();                                \
}                                          
//-------------------------------------------------


//-------------------------------------------------
// Defines
//-------------------------------------------------
#define MAX_LENGTH 20
//-------------------------------------------------
#define WS_GAMEWINDOW (WS_OVERLAPPED  |              \
	                   WS_CAPTION     |              \
                       WS_SYSMENU     |				 \
					   WS_EX_TOPMOST  |              \
                       WS_MINIMIZEBOX)
//-------------------------------------------------


//-------------------------------------------------
// Typedefes
//-------------------------------------------------
typedef bool (CALLBACK*Func)();
//-------------------------------------------------


namespace mSystem
{
	enum Cursor { DEFAULT = 0, MODIFY = 1 };
	
	class Application
	{	
	public:
		Application();
		~Application();

		HRESULT Create_Window( char *Name, UINT width, UINT height, int flag );

		HWND      GetWnd();
		HINSTANCE GetInst();
		UINT      GetWidth();
		UINT      GetHeight();

		void    ShowMouse( bool Show );
		HRESULT SetWindowCursor( LPCSTR CursorName );

		HRESULT Run();

		void End();

		void SetRunGameFunction( Func ExFunc );
		void SetInitGameFunction( Func ExFunc );
		void SetEndGameFunction( Func ExFunc );

	protected:
		static LRESULT CALLBACK WndProc( HWND, UINT, WPARAM, LPARAM );

		WNDCLASSEX wc;

		char WinName[MAX_LENGTH];

		UINT width;
		UINT height;

		Func RunGame;
		Func InitGame;
		Func EndGame;

	private:
		HWND hWnd;
		HINSTANCE hInst;
	};

	class StateManager
	{
	public:
		StateManager();
		~StateManager();

		void Add( void* Object, void (*mFunc)( void* Obj ) );
		BOOL Del();
		void DelAll();
		HRESULT Process();

	protected:
		struct State
		{
			State()
			{
				Next = NULL;
				Function = NULL;
				Obj = NULL;
			}

			~State()
			{
				delete Next;
			}
			
			void (*Function)( void* Obj );
			void* Obj;

			State *Next;
		};

		State *pActiveState;
	};

	class ProcessManager
	{
	public:
		ProcessManager();
		~ProcessManager();

		void Add( void* Object, void (*mFunc)( void* Obj ) );
		BOOL Del();
		void DelAll();
		HRESULT Run();

		static void Run( void* Obj );

	protected:
		struct Process
		{
			Process()
			{
				Next = NULL;
				Function = NULL;
			}

			~Process()
			{
				delete Next;
			}

			void (*Function)( void* Obj );
			void* Obj;

			Process *Next;
		};

		Process *pFirstProcess;
	};

	class DataPackage
	{
	public:
		DataPackage();
		~DataPackage();

		void* Create( UINT size );
		void Release();

		BOOL Save( char *FileName );
		void* Load( char *FileName, UINT *size );

		UINT GetSize();
		void* GetData();

	protected:
		UINT Size;
		void* Data;
	};

	class Settings
	{
	public:
		Settings( string file );
		~Settings();

		int GetData( string DataName );

	protected:
		map<string, int> Data;
	};
};

#endif