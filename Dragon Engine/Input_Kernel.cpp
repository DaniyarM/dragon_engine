/**************************************************
Dragon Engine 0.1

Input_Kernel (.cpp)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/

//-------------------------------------------------
// Includes
//-------------------------------------------------
#include "Input_Kernel.h"
//-------------------------------------------------

////////Device////////

Input::Device::Device( mSystem::Application* App )
{
	pApplication = App;

	pDirectInput = NULL;

	if( App != NULL )
		Create();
}

Input::Device::~Device()
{
	//delete pApplication;

	//delete pDirectInput;
}

void Input::Device::SetApp( mSystem::Application* App )
{
	pApplication = App;
}

HRESULT Input::Device::Create()
{
	V_RETURN( DirectInput8Create( pApplication->GetInst(), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&pDirectInput, NULL ) );
}

IDirectInput8* Input::Device::GetDevice()
{
	return pDirectInput;
}

HWND Input::Device::GetWnd()
{
	return pApplication->GetWnd();
}

////////Keybord////////

Input::Keybord::Keybord( Device* Device )
{
	ZeroMemory( buttons, sizeof( buttons ) );
	ZeroMemory( lock, sizeof( lock ) );
	
	pDevice = Device;

	pKeybord = NULL;

	pDevice->GetDevice()->CreateDevice( GUID_SysKeyboard, &pKeybord, NULL );

	pKeybord->SetDataFormat( &c_dfDIKeyboard );
	pKeybord->SetCooperativeLevel( pDevice->GetWnd(), DISCL_FOREGROUND | DISCL_EXCLUSIVE );
	pKeybord->Acquire();
}

Input::Keybord::~Keybord()
{
	//delete pDevice;

	//delete pKeybord;
}

bool Input::Keybord::GetButton( char num )
{
	return ( ( ( buttons[num] & 0x80 ) && !lock[num] ) ? true : false );
}

void Input::Keybord::BlockButton( char num, bool block )
{
	lock[num] = block;
}

HRESULT Input::Keybord::Update()
{
	V_RETURN( pKeybord->GetDeviceState( sizeof( buttons ), buttons ) );
}

void Input::Keybord::Update( void* Obj )
{
	Keybord* mKeybord = (Keybord*)Obj;

	if( FAILED( mKeybord->Update() ) )
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ���������� ������ Input::Keybord!" );
}

////////Mouse////////

Input::Mouse::Mouse( Device* Device )
{
	ZeroMemory( &buttons, sizeof( buttons ) );
	ZeroMemory( lock, sizeof( lock ) );
	
	pDevice = Device;

	pMouse = NULL;

	pDevice->GetDevice()->CreateDevice( GUID_SysMouse, &pMouse, NULL );

	pMouse->SetDataFormat( &c_dfDIMouse );
	pMouse->SetCooperativeLevel( pDevice->GetWnd(), DISCL_FOREGROUND | DISCL_EXCLUSIVE );
	pMouse->Acquire();
}

Input::Mouse::~Mouse()
{
	//delete pDevice;

	//delete pMouse;
}

long long Input::Mouse::GetButton( int num )
{
	switch( num )
	{
	case M_X:
		if( !lock[M_X] )
		    return buttons.lX;
	case M_Y:
		if( !lock[M_Y] )
		    return buttons.lY;
	case M_Z:
		if( !lock[M_Z] )
		    return buttons.lZ;
	case M_B1:
		return ( ( buttons.rgbButtons[0] & 0x80 ) && !lock[M_B1] );
	case M_B2:
		return ( ( buttons.rgbButtons[1] & 0x80 ) && !lock[M_B2] );
	case M_B3:
		return ( ( buttons.rgbButtons[2] & 0x80 ) && !lock[M_B3] );
	case M_B4:
		return ( ( buttons.rgbButtons[3] & 0x80 ) && !lock[M_B4] );
	}
}

void Input::Mouse::BlockButton( int num, bool block )
{
	lock[num] = block;
}

HRESULT Input::Mouse::Update()
{
	V_RETURN( pMouse->GetDeviceState( sizeof( buttons ), &buttons ) );
}

void Input::Mouse::Update( void* Obj )
{
	Mouse* mMouse = (Mouse*)Obj;

	if( FAILED( mMouse->Update() ) )
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ���������� ������ Input::Keybord!" );
}