/**************************************************
Dragon Engine 0.1

Input_Kernel (.h)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/
#ifndef INPUT_KERNEL_H
#define INPUT_KERNEL_H

//-------------------------------------------------
// Debug settings
//-------------------------------------------------
#define SHOW_ERRORS 1
//-------------------------------------------------


//-------------------------------------------------
// Warnings disable
//-------------------------------------------------
#pragma warning ( disable : 4005 )
#pragma warning ( disable : 4018 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4305 )
#pragma warning ( disable : 4715 )
#pragma warning ( disable : 4996 )
#pragma warning ( disable : 4800 )
//#define _CRT_SECURE_NO_WARNINGS
//-------------------------------------------------


//-------------------------------------------------
// Includes
//------------------------------------------------
#include <dinput.h>
//------------------------------------------------


//-------------------------------------------------
// Include mandatory kernels 
//-------------------------------------------------
#include "System_Kernel.h"
//-------------------------------------------------


//-------------------------------------------------
// Macroses
//-------------------------------------------------
#define MsgBox( tit, mes ) MessageBox( NULL, mes, tit, MB_OK );
//-------------------------------------------------
#define V_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return E_FAIL;                               \
	}                                                \
}
//-------------------------------------------------
#define C_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return 1;                                    \
	}                                                \
}
//-------------------------------------------------
#define SAVE_RELEASE( x )                            \
{                                                    \
	if( x )                                          \
		x->Release();                                \
}                                          
//-------------------------------------------------


//-------------------------------------------------
// Typedefes
//-------------------------------------------------
typedef bool (CALLBACK*Func)();
//-------------------------------------------------


//-------------------------------------------------
// Defines
//-------------------------------------------------
#define M_X  0
#define M_Y  1
#define M_Z  2
#define M_B1 3
#define M_B2 4
#define M_B3 5
#define M_B4 6
//-------------------------------------------------

namespace Input
{
	class Device
	{
	public:
		Device( mSystem::Application* App = NULL );
		~Device();

		void SetApp( mSystem::Application* App );

		HRESULT Create();

		IDirectInput8* GetDevice();
		HWND GetWnd();

	protected:
		mSystem::Application* pApplication;
		
        IDirectInput8 *pDirectInput;
	};
	
	class Keybord
	{
	public:
		Keybord( Device* Device );
		~Keybord();

		bool GetButton( char num );
		void BlockButton( char num, bool block );

		static void Update( void* Obj );

	protected:
		HRESULT Update();

		Device* pDevice;

		IDirectInputDevice8* pKeybord;

		char buttons[256];
		bool lock[256];
	};

	class Mouse
	{
	public:
		Mouse( Device* Device );
		~Mouse();

		long long GetButton( int num );
		void BlockButton( int num, bool block );

		static void Update( void* Obj );

	protected:
		HRESULT Update();

		Device* pDevice;

		IDirectInputDevice8* pMouse;

		DIMOUSESTATE buttons;
		bool lock[7];
	};
};

#endif