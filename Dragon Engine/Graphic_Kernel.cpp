/**************************************************
Dragon Engine 0.1

Graphic_Kernel (.cpp)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/

//-------------------------------------------------
// Includes
//-------------------------------------------------
#include "Graphic_Kernel.h"
//-------------------------------------------------


////////Device3D////////

Graphic::Device3D::Device3D( mSystem::Application* App )
{
	Application = App;
	
	hWnd              = NULL;
	Fullscreen        = false;

	DriverType        = D3D_DRIVER_TYPE_NULL;
	FeatureLevel      = D3D_FEATURE_LEVEL_11_0;

	pSwapChain        = NULL;
	pDevice           = NULL;
	pDeviceContext    = NULL;
	pRenderTargetView = NULL;

	OnCreateDevice    = NULL;
	OnDestroyDevice   = NULL;
	OnFrameMove       = NULL;
	OnFrameRender     = NULL;
	OnResizeSwapChain = NULL;
}

Graphic::Device3D::~Device3D()
{
	SAVE_RELEASE( pDevice );

	SAVE_RELEASE( pDeviceContext );

	SAVE_RELEASE( pSwapChain );

	SAVE_RELEASE( pRenderTargetView );

	SAVE_RELEASE( pDXGIFactory );

	SAVE_RELEASE( pDXGIDevice );

	SAVE_RELEASE( pDXGIAdapter );

	if( OnDestroyDevice != NULL )
	    if( (*OnDestroyDevice)() )
			if( SHOW_ERRORS )
	            MsgBox( "������������� ������", "������ � ������� OnDestroyDevice()!" );
}

void Graphic::Device3D::SetCreateDeviceFunction( Func CreateDevice )
{
	OnCreateDevice = CreateDevice;
}

void Graphic::Device3D::SetDestroyDeviceFunction( Func DestroyDevice )
{
	OnDestroyDevice = DestroyDevice;
}

void Graphic::Device3D::SetFrameMoveFunction( Func FrameMove )
{
	OnFrameMove = FrameMove;
}

void Graphic::Device3D::SetFrameRenderFunction( Func FrameRender )
{
	OnFrameRender = FrameRender;
}

void Graphic::Device3D::SetResizeSwapChainFunction( Func ResizeSwapChain )
{
	OnResizeSwapChain = ResizeSwapChain;
}

HRESULT Graphic::Device3D::Create( bool fullscreen )
{
	hWnd = Application->GetWnd();
	width  = Application->GetWidth();
	height = Application->GetHeight();
	
	Fullscreen = fullscreen;

	D3D_DRIVER_TYPE driverTypes[] = 
	{ 
		D3D_DRIVER_TYPE_HARDWARE, 
		D3D_DRIVER_TYPE_WARP, 
		D3D_DRIVER_TYPE_REFERENCE 
	};

	UINT numDriverTypes = sizeof( driverTypes ) / sizeof( driverTypes[0] ); // ARRAYSIZE( driverTypes )

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};

	UINT numFeatureLevels = sizeof( featureLevels ) / sizeof( featureLevels[0] ); // ARRAYSIZE( featureLevels )
	
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory( &sd, sizeof( sd ) );

	sd.BufferCount                        = 1;
	sd.BufferDesc.Width                   = width;
	sd.BufferDesc.Height                  = height;
	sd.BufferDesc.Format                  = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator   = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.ScanlineOrdering        = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling			      = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.BufferUsage                        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow                       = hWnd;
	sd.SampleDesc.Count                   = 1;
	sd.SampleDesc.Quality                 = 0;
	sd.Windowed                           = true;
	sd.SwapEffect                         = DXGI_SWAP_EFFECT_DISCARD;

	for( UINT driverIndex = 0; driverIndex < numDriverTypes; driverIndex++ )
	{
		DriverType = driverTypes[driverIndex];

		HRESULT hr = D3D11CreateDeviceAndSwapChain( NULL, DriverType, NULL, 0, featureLevels, numFeatureLevels, D3D11_SDK_VERSION, 
			                                        &sd, &pSwapChain, &pDevice, &FeatureLevel, &pDeviceContext );

		if( SUCCEEDED( hr ) )
			break;
		else
			if( driverIndex + 1 == numDriverTypes )
			{
				if( SHOW_ERRORS )
					MsgBox( "������!", "������ � ������� D3D11CreateDeviceAndSwapChain()!" );
				return E_FAIL;
			}
	}

	ID3D11Texture2D *pBackBuffer;

	V_RETURN( pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID*)&pBackBuffer ) );

	V_RETURN( pDevice->CreateRenderTargetView( pBackBuffer, NULL, &pRenderTargetView ) );

	pBackBuffer->Release();

	//DepthStencilView
	ID3D11Texture2D *pDepthStencil;
	
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory( &descDepth, sizeof( descDepth ) );

	descDepth.Width              = width;
	descDepth.Height             = height;
	descDepth.MipLevels          = 1;
	descDepth.ArraySize          = 1;
	descDepth.Format             = DXGI_FORMAT_D32_FLOAT;
	descDepth.SampleDesc.Count   = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.BindFlags          = D3D11_BIND_DEPTH_STENCIL;

	V_RETURN( pDevice->CreateTexture2D( &descDepth, NULL, &pDepthStencil ) );

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory( &descDSV, sizeof( descDSV ) );

	descDSV.Format             = descDepth.Format;
	descDSV.ViewDimension      = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	V_RETURN( pDevice->CreateDepthStencilView( pDepthStencil, &descDSV, &pDepthStencilView ) );

	pDepthStencil->Release();

	pDeviceContext->OMSetRenderTargets( 1, &pRenderTargetView, pDepthStencilView );

	D3D11_VIEWPORT vp;

	vp.Width    = width;
	vp.Height   = height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	
	pDeviceContext->RSSetViewports( 1, &vp );

	// No Alt+Enter FullScreen switching
	V_RETURN( pDevice->QueryInterface( __uuidof( IDXGIDevice ), (void **)&pDXGIDevice ) ); 
	V_RETURN( pDXGIDevice->GetParent( __uuidof( IDXGIAdapter ), (void **)&pDXGIAdapter ) );
	V_RETURN( pDXGIAdapter->GetParent( __uuidof( IDXGIFactory ), (void **)&pDXGIFactory ) );

	pDXGIFactory->MakeWindowAssociation( hWnd, DXGI_MWA_NO_ALT_ENTER );

	// Set FullScreen mode
	if( fullscreen )
	    FullScreen( fullscreen );

	if( OnCreateDevice != NULL )
	{
	    if( (*OnCreateDevice)() )
			if( SHOW_ERRORS )
	            MsgBox( "������������� ������", "������ � ������� OnCreateDevice()!" );
	}

	return S_OK;
}

HRESULT Graphic::Device3D::FullScreen( bool fullscreen )
{
	Fullscreen = fullscreen;

	if( fullscreen )
		width = Application->GetWidth(), height = Application->GetHeight();
	else
		width = GetSystemMetrics( SM_CXSCREEN ), height = GetSystemMetrics( SM_CYSCREEN );
	
	V_RETURN( pSwapChain->SetFullscreenState( fullscreen, NULL ) );

	if( pRenderTargetView )
		pRenderTargetView->Release();

	if( pSwapChain )
		pSwapChain->ResizeBuffers( 2, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0 );

	ID3D11Texture2D *pBackBuffer;

	V_RETURN( pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID*)&pBackBuffer ) );

	V_RETURN( pDevice->CreateRenderTargetView( pBackBuffer, NULL, &pRenderTargetView ) );

	pBackBuffer->Release();

	//DepthStencilView
	pDepthStencilView->Release();

	ID3D11Texture2D *pDepthStencil;
	
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory( &descDepth, sizeof( descDepth ) );

	descDepth.Width              = width;
	descDepth.Height             = height;
	descDepth.MipLevels          = 1;
	descDepth.ArraySize          = 1;
	descDepth.Format             = DXGI_FORMAT_D32_FLOAT;
	descDepth.SampleDesc.Count   = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.BindFlags          = D3D11_BIND_DEPTH_STENCIL;

	V_RETURN( pDevice->CreateTexture2D( &descDepth, NULL, &pDepthStencil ) );

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory( &descDSV, sizeof( descDSV ) );

	descDSV.Format             = descDepth.Format;
	descDSV.ViewDimension      = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	V_RETURN( pDevice->CreateDepthStencilView( pDepthStencil, &descDSV, &pDepthStencilView ) );

	pDeviceContext->OMSetRenderTargets( 1, &pRenderTargetView, pDepthStencilView );

	D3D11_VIEWPORT vp;

	vp.Width    = width;
	vp.Height   = height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	
	pDeviceContext->RSSetViewports( 1, &vp );

	if( OnResizeSwapChain != NULL )
	    if( (*OnResizeSwapChain)() )
			if( SHOW_ERRORS )
	            MsgBox( "������������� ������", "������ � ������� OnResizeSwapChain()!" );
	
	return S_OK;
}

HRESULT Graphic::Device3D::Frame()
{
	if( OnFrameMove == NULL || OnFrameRender == NULL )
	{
		if( SHOW_ERRORS )
			MsgBox( "������!", "�� ��������� ������� ���������� ���� ������� ��� ����������!" );
		
		return E_FAIL;
	}
	
	if( (*OnFrameMove)() )
	{
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ������� OnFrameMove()!" );
		
		return E_FAIL;
	}

	if( (*OnFrameRender)() )
	{
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ������� OnFrameRender()!" );
		
		return E_FAIL;
	}
	
	return S_OK;
}

IDXGISwapChain* Graphic::Device3D::GetSwapChain()
{
	return pSwapChain;
}

ID3D11Device* Graphic::Device3D::GetDevice()
{
	return pDevice;
}

ID3D11DeviceContext* Graphic::Device3D::GetDeviceContext()
{
	return pDeviceContext;
}

ID3D11RenderTargetView* Graphic::Device3D::GetRenderTargetView()
{
	return pRenderTargetView;
}

ID3D11DepthStencilView* Graphic::Device3D::GetDepthStencilView()
{
	return pDepthStencilView;
}

IDXGIDevice* Graphic::Device3D::GetDXGIDevice()
{
	return pDXGIDevice;
}

IDXGIAdapter* Graphic::Device3D::GetDXGIAdapter()
{
	return pDXGIAdapter;
}

IDXGIFactory* Graphic::Device3D::GetDXGIFactory()
{
	return pDXGIFactory;
}

void Graphic::Device3D::ClearAll( float* color )
{
	ClearRTV( color );
	ClearDSV();
}

void Graphic::Device3D::ClearRTV( float* color )
{	
	if( !color )
	{
		//float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // RGBA
		float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // RGBA
		color = ClearColor;
	}
	
	pDeviceContext->ClearRenderTargetView( pRenderTargetView, color );
}

void Graphic::Device3D::ClearDSV()
{
	pDeviceContext->ClearDepthStencilView( pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0 );
}

void Graphic::Device3D::Present()
{
	pSwapChain->Present( 0, 0 );
}

UINT Graphic::Device3D::GetWidth()
{
	return width;
}

UINT Graphic::Device3D::GetHeight()
{
	return height;
}

D3D_DRIVER_TYPE Graphic::Device3D::GetDriverType()
{
	return DriverType;
}

D3D_FEATURE_LEVEL Graphic::Device3D::GetFeatureLevel()
{
	return FeatureLevel;
}

float Graphic::Device3D::GetTime()
{
	if( DriverType == D3D_DRIVER_TYPE_REFERENCE )
    {
        static float t = 0;
		t += 0.0125f;
		return t;
    }
	
	static DWORD TimeStart = 0;
	DWORD        TimeCur   = GetTickCount();

	if( TimeStart == 0 )
	{
		TimeStart = TimeCur;

		return 0.0f;
	}

	return (TimeCur - TimeStart) / 1000.0f;
}

void Graphic::Device3D::Frame( void* Obj )
{
	Device3D* mDevice = (Device3D*)Obj;

	if( FAILED( mDevice->Frame() ) )
		if( SHOW_ERRORS )
			MsgBox( "������!", "������ � ���������� ������ Graphic::Device3D!" );
}

////////Shader////////

Graphic::Shader::Shader( Graphic::Device3D* Device )
{
	Device3D = Device;

	VS = NULL;
	PS = NULL;
	HS = NULL;
	DS = NULL;
	GS = NULL;
	CS = NULL;

	VS_Buffer = NULL;
	PS_Buffer = NULL;
	HS_Buffer = NULL;
	DS_Buffer = NULL;
	GS_Buffer = NULL;
	CS_Buffer = NULL;
}

Graphic::Shader::~Shader()
{
	SAVE_RELEASE( VS );
	SAVE_RELEASE( PS );
	SAVE_RELEASE( HS );
	SAVE_RELEASE( DS );
	SAVE_RELEASE( GS );
	SAVE_RELEASE( CS );

	SAVE_RELEASE( VS_Buffer );
	SAVE_RELEASE( PS_Buffer );
	SAVE_RELEASE( HS_Buffer );
	SAVE_RELEASE( DS_Buffer );
	SAVE_RELEASE( GS_Buffer );
	SAVE_RELEASE( CS_Buffer );
}

HRESULT Graphic::Shader::Create( char* file, char* version, char* VertexShaderName, char* PixelShaderName, char* HullShaderName, char* DomainShaderName, 
								   char* GeometryShaderName, char* ComputeShaderName )
{
	char VSVersion[10] = "vs_", PSVersion[10] = "ps_", HSVersion[10] = "hs_", DSVersion[10] = "ds_", GSVersion[10] = "gs_", CSVersion[10] = "cs_";

	strcat ( VSVersion, version );
	strcat ( PSVersion, version );
	strcat ( HSVersion, version );
	strcat ( DSVersion, version );
	strcat ( GSVersion, version );
	strcat ( CSVersion, version );
	
	if( VertexShaderName )
	{
		V_RETURN( D3DX11CompileFromFile( file, 0, 0, VertexShaderName, VSVersion, D3D10_SHADER_PACK_MATRIX_ROW_MAJOR, 0, 0, &VS_Buffer, 0, 0 ) );
		V_RETURN( Device3D->GetDevice()->CreateVertexShader( VS_Buffer->GetBufferPointer(), VS_Buffer->GetBufferSize(), NULL, &VS ) );
	}

	VSBufferPointer = VS_Buffer->GetBufferPointer();
	VSBufferSize    = VS_Buffer->GetBufferSize();

	if( PixelShaderName )
	{
		V_RETURN( D3DX11CompileFromFile( file, 0, 0, PixelShaderName, PSVersion, D3D10_SHADER_PACK_MATRIX_ROW_MAJOR, 0, 0, &PS_Buffer, 0, 0 ) );
		V_RETURN( Device3D->GetDevice()->CreatePixelShader( PS_Buffer->GetBufferPointer(), PS_Buffer->GetBufferSize(), NULL, &PS ) );
	}

	if( HullShaderName )
	{
		V_RETURN( D3DX11CompileFromFile( file, 0, 0, HullShaderName, HSVersion, D3D10_SHADER_PACK_MATRIX_ROW_MAJOR, 0, 0, &HS_Buffer, 0, 0 ) );
		V_RETURN( Device3D->GetDevice()->CreateHullShader( HS_Buffer->GetBufferPointer(), HS_Buffer->GetBufferSize(), NULL, &HS ) );
	}

	if( DomainShaderName )
	{
		V_RETURN( D3DX11CompileFromFile( file, 0, 0, DomainShaderName, DSVersion, D3D10_SHADER_PACK_MATRIX_ROW_MAJOR, 0, 0, &DS_Buffer, 0, 0 ) );
		V_RETURN( Device3D->GetDevice()->CreateDomainShader( DS_Buffer->GetBufferPointer(), DS_Buffer->GetBufferSize(), NULL, &DS ) );
	}

	if( GeometryShaderName )
	{
		V_RETURN( D3DX11CompileFromFile( file, 0, 0, GeometryShaderName, GSVersion, D3D10_SHADER_PACK_MATRIX_ROW_MAJOR, 0, 0, &HS_Buffer, 0, 0 ) );
		V_RETURN( Device3D->GetDevice()->CreateGeometryShader( GS_Buffer->GetBufferPointer(), GS_Buffer->GetBufferSize(), NULL, &GS ) );
	}

	if( ComputeShaderName )
	{
		V_RETURN( D3DX11CompileFromFile( file, 0, 0, ComputeShaderName, CSVersion, D3D10_SHADER_PACK_MATRIX_ROW_MAJOR, 0, 0, &CS_Buffer, 0, 0 ) );
		V_RETURN( Device3D->GetDevice()->CreateComputeShader( CS_Buffer->GetBufferPointer(), CS_Buffer->GetBufferSize(), NULL, &CS ) );
	}
	
	return S_OK;
}

void Graphic::Shader::SetConstantBufferVS( ID3D11Buffer* ObjectBuffer )
{
	Device3D->GetDeviceContext()->VSSetConstantBuffers( 0, 1, &ObjectBuffer );
}

void Graphic::Shader::SetConstantBufferPS( ID3D11Buffer* ObjectBuffer )
{
	Device3D->GetDeviceContext()->PSSetConstantBuffers( 0, 1, &ObjectBuffer );
}

void Graphic::Shader::SetConstantBufferHS( ID3D11Buffer* ObjectBuffer )
{
	Device3D->GetDeviceContext()->HSSetConstantBuffers( 0, 1, &ObjectBuffer );
}

void Graphic::Shader::SetConstantBufferDS( ID3D11Buffer* ObjectBuffer )
{
	Device3D->GetDeviceContext()->DSSetConstantBuffers( 0, 1, &ObjectBuffer );
}

void Graphic::Shader::SetConstantBufferGS( ID3D11Buffer* ObjectBuffer )
{
	Device3D->GetDeviceContext()->GSSetConstantBuffers( 0, 1, &ObjectBuffer );
}

void Graphic::Shader::SetConstantBufferCS( ID3D11Buffer* ObjectBuffer )
{
	Device3D->GetDeviceContext()->CSSetConstantBuffers( 0, 1, &ObjectBuffer );
}

void Graphic::Shader::SetResourceViewAndSamplerState( ID3D11ShaderResourceView* constant, ID3D11SamplerState* SamplerState )
{
	Device3D->GetDeviceContext()->PSSetShaderResources( 0, 1, &constant );
	
	Device3D->GetDeviceContext()->PSSetSamplers( 0, 1, &SamplerState );
}

void Graphic::Shader::Apply()
{
	 if( VS )
		Device3D->GetDeviceContext()->VSSetShader( VS, 0, 0 );

	 if( PS )
		 Device3D->GetDeviceContext()->PSSetShader( PS, 0, 0 );

	 if( HS )
		Device3D->GetDeviceContext()->HSSetShader( HS, 0, 0 );

	 if( DS )
		Device3D->GetDeviceContext()->DSSetShader( DS, 0, 0 );

	 if( GS )
		Device3D->GetDeviceContext()->GSSetShader( GS, 0, 0 );

	 if( CS )
		Device3D->GetDeviceContext()->CSSetShader( CS, 0, 0 );
}

ID3D10Blob* Graphic::Shader::GetVSBuffer()
{
	return VS_Buffer;
}

////////Camera////////

Graphic::Camera::Camera( Graphic::Device3D* Device )
{
	Device3D = Device;

	Eye.x = 0;
	Eye.y = 0;
	Eye.z = 0;

	LookAt.x = 0;
	LookAt.y = 0;
	LookAt.z = 1;

	Up.x = 0;
	Up.y = 1;
	Up.z = 0;

	//mView = XMMatrixIdentity();
	
	mView = XMMatrixLookAtLH( XMVectorSet( Eye.x, Eye.y, Eye.z, 0 ), XMVectorSet( LookAt.x, LookAt.y, LookAt.z, 0 ), XMVectorSet( Up.x, Up.y, Up.z, 0 ) );
	
	//mProjection = XMMatrixIdentity();
	
	//UpdatePerspective();
	mProjection = XMMatrixIdentity();

	RotationX = 0;
	RotationY = 0;
}

Graphic::Camera::~Camera()
{
	// EMPTY
}

void Graphic::Camera::UpdatePerspective()
{
	mProjection = XMMatrixPerspectiveFovLH( (float)XM_PI/4, (float)(Device3D->GetWidth() / Device3D->GetHeight()), 0.1f, 1000.0f );
}

void Graphic::Camera::TranslationX( float trans )
{
	XMMATRIX Trans = XMMatrixTranslation( XMConvertToRadians( trans ), 0, 0 );
	mView = XMMatrixMultiply( mView, Trans );
}

void Graphic::Camera::TranslationY( float trans )
{
	XMMATRIX Trans = XMMatrixTranslation( 0, XMConvertToRadians( trans ), 0 );
	mView = XMMatrixMultiply( mView, Trans );
}

void Graphic::Camera::TranslationZ( float trans )
{
	XMMATRIX RotateX = XMMatrixRotationX( XMConvertToRadians( -RotationX ) );
	mView = XMMatrixMultiply( mView, RotateX );
	
	XMMATRIX Trans = XMMatrixTranslation( 0, 0, XMConvertToRadians( trans ) );
	mView = XMMatrixMultiply( mView, Trans );

	RotateX = XMMatrixRotationX( XMConvertToRadians( RotationX ) );
	mView = XMMatrixMultiply( mView, RotateX );
}

void Graphic::Camera::RotateX( float trans )
{
	if( RotationX + trans > 80 || RotationX + trans < -80 )
		return;

	RotationX += trans;
	
	XMMATRIX Rotate = XMMatrixRotationX( XMConvertToRadians( trans ) );
	mView = XMMatrixMultiply( mView, Rotate );
}

void Graphic::Camera::RotateY( float trans )
{
	RotationY += trans;

	if( RotationY >= 360 )
		RotationY -= 360;

	if( RotationY <= -360 )
		RotationY += 360;
	
	XMMATRIX RotateX = XMMatrixRotationX( XMConvertToRadians( -RotationX ) );
	mView = XMMatrixMultiply( mView, RotateX );
	
	XMMATRIX Rotate = XMMatrixRotationY( XMConvertToRadians( trans ) );
	mView = XMMatrixMultiply( mView, Rotate );

	RotateX = XMMatrixRotationX( XMConvertToRadians( RotationX ) );
	mView = XMMatrixMultiply( mView, RotateX );
}

void Graphic::Camera::RotateZ( float trans )
{	
	XMMATRIX Rotate = XMMatrixRotationZ( XMConvertToRadians( trans ) );
	mView = XMMatrixMultiply( mView, Rotate );
}

XMMATRIX Graphic::Camera::GetView()
{
	return mView;
}

XMMATRIX Graphic::Camera::GetProjection()
{
	return mProjection;
}

////////Text////////

Graphic::Text::Text( Graphic::Device3D* Device, Graphic::Shader* Shader )
{
	Characters.clear();

	Device3D     = Device;
	Text::Shader = Shader;

	VertexBuffer = NULL;
	vertLayout   = NULL;

	Texture         = NULL;
	TexSamplerState = NULL;
	ObjectBuffer    = NULL;
}

Graphic::Text::~Text()
{
	Characters.clear();

	SAVE_RELEASE( VertexBuffer );
	SAVE_RELEASE( vertLayout );

	SAVE_RELEASE( Texture );
	SAVE_RELEASE( TexSamplerState );
	SAVE_RELEASE( ObjectBuffer );
}

HRESULT Graphic::Text::Create( char* name )
{
	// BlenderState
	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
    ZeroMemory( &rtbd, sizeof(rtbd) );

    rtbd.BlendEnable           = true;
    rtbd.BlendOp               = D3D11_BLEND_OP_ADD;
    rtbd.SrcBlend              = D3D11_BLEND_SRC_ALPHA;
    rtbd.DestBlend             = D3D11_BLEND_INV_SRC_ALPHA;
    rtbd.DestBlendAlpha        = D3D11_BLEND_ONE;
    rtbd.SrcBlendAlpha         = D3D11_BLEND_ONE;
    rtbd.BlendOpAlpha          = D3D11_BLEND_OP_ADD;
    rtbd.RenderTargetWriteMask = D3D10_COLOR_WRITE_ENABLE_ALL;

    D3D11_BLEND_DESC blendDesc;
    ZeroMemory( &blendDesc, sizeof(blendDesc) );

    blendDesc.AlphaToCoverageEnable = false;
    blendDesc.RenderTarget[0] = rtbd;

    Device3D->GetDevice()->CreateBlendState( &blendDesc, &Transparency );
	
	vertices = new TextVertex[6*1000];
    ZeroMemory( vertices, sizeof( TextVertex ) * 6*1000 );

    D3D11_BUFFER_DESC vertexBufferDesc;
    ZeroMemory( &vertexBufferDesc, sizeof( vertexBufferDesc ) );

    vertexBufferDesc.Usage               = D3D11_USAGE_DYNAMIC;
    vertexBufferDesc.ByteWidth           = sizeof( TextVertex ) * 6 * 1000;
    vertexBufferDesc.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
    vertexBufferDesc.MiscFlags           = 0;
    vertexBufferDesc.StructureByteStride = 0;

    D3D11_SUBRESOURCE_DATA vertexBufferData;
    ZeroMemory( &vertexBufferData, sizeof( vertexBufferData ) );

    vertexBufferData.pSysMem          = vertices;
    vertexBufferData.SysMemPitch      = 0;
    vertexBufferData.SysMemSlicePitch = 0; 

	V_RETURN( Device3D->GetDevice()->CreateBuffer( &vertexBufferDesc, &vertexBufferData, &VertexBuffer ) );

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR",    0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
    UINT num = ARRAYSIZE( layout );

	V_RETURN( Device3D->GetDevice()->CreateInputLayout( layout, num, Shader->GetVSBuffer()->GetBufferPointer(), Shader->GetVSBuffer()->GetBufferSize(), 
		      &vertLayout  ));

	V_RETURN( D3DX11CreateShaderResourceViewFromFile( Device3D->GetDevice(), name, NULL, NULL, &Texture, NULL ) );

	D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory( &sampDesc, sizeof( sampDesc ) );

    sampDesc.Filter         = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD         = 0;
    sampDesc.MaxLOD         = D3D11_FLOAT32_MAX;

    V_RETURN( Device3D->GetDevice()->CreateSamplerState( &sampDesc, &TexSamplerState ) );

	D3D11_BUFFER_DESC cbbd;
    ZeroMemory( &cbbd, sizeof( D3D11_BUFFER_DESC ) ); 

    cbbd.Usage = D3D11_USAGE_DEFAULT;
    cbbd.ByteWidth = sizeof( XMMATRIX );
    cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbbd.CPUAccessFlags = 0;
    cbbd.MiscFlags = 0;

    V_RETURN( Device3D->GetDevice()->CreateBuffer( &cbbd, NULL, &ObjectBuffer ) );

	return S_OK;
}

bool Graphic::Text::Draw( string text, UINT size, XMFLOAT4 color, UINT left, UINT top )
{
	for( int i=0; i<text.size(); i++ )
		Characters.push_back( TextCharacter( text[i], size, left+i*size, top, color) );

	TextCharacter CurChar;
	int row, pitch;
	XMFLOAT4 CurColor;

	for( unsigned int i=0; i<Characters.size(); i++ )
	{
		CurChar = GetCharacter( i );
		unsigned char s = CurChar.symbol;

		row = ((int)s-1) % 16;
		pitch = ((int)s-row-1) / 16;
		CurColor = (XMFLOAT4)CurChar.color;

		vertices[6*i + 0] = TextVertex( CurChar.x-Device3D->GetWidth()/2,              -CurChar.y+Device3D->GetHeight()/2-CurChar.size, 0.0f, (row*10)/160.0f,     (pitch+1)*19/304.0f,  CurColor );
		vertices[6*i + 1] = TextVertex( CurChar.x-Device3D->GetWidth()/2,              -CurChar.y+Device3D->GetHeight()/2,              0.0f, (row*10)/160.0f,      pitch*19/304.0f,     CurColor );
		vertices[6*i + 2] = TextVertex( CurChar.x-Device3D->GetWidth()/2+CurChar.size, -CurChar.y+Device3D->GetHeight()/2-CurChar.size, 0.0f, ((row+1)*10)/160.0f, (pitch+1)*19/304.0f,  CurColor );
		vertices[6*i + 3] = TextVertex( CurChar.x-Device3D->GetWidth()/2,              -CurChar.y+Device3D->GetHeight()/2,              0.0f, (row*10)/160.0f,      pitch*19/304.0f,     CurColor );
		vertices[6*i + 4] = TextVertex( CurChar.x-Device3D->GetWidth()/2+CurChar.size, -CurChar.y+Device3D->GetHeight()/2,              0.0f, ((row+1)*10)/160.0f,  pitch*19/304.0f,     CurColor );
		vertices[6*i + 5] = TextVertex( CurChar.x-Device3D->GetWidth()/2+CurChar.size, -CurChar.y+Device3D->GetHeight()/2-CurChar.size, 0.0f, ((row+1)*10)/160.0f, (pitch+1)*19/304.0f,  CurColor );
	}

	D3D11_MAPPED_SUBRESOURCE mappedResource;

    V_RETURN( Device3D->GetDeviceContext()->Map( VertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource ) );

	TextVertex* verticesPtr = (TextVertex*)mappedResource.pData;
    memcpy( verticesPtr, (void*)vertices, (sizeof( TextVertex ) * 6 * Characters.size()) );

    Device3D->GetDeviceContext()->Unmap( VertexBuffer, 0 );

	Shader->Apply();

	UINT stride = sizeof( TextVertex );
    UINT offset = 0;
    Device3D->GetDeviceContext()->IASetVertexBuffers( 0, 1, &VertexBuffer, &stride, &offset );
    Device3D->GetDeviceContext()->IASetInputLayout( vertLayout );
    Device3D->GetDeviceContext()->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	Shader->SetResourceViewAndSamplerState( Texture, TexSamplerState );

    Device3D->GetDeviceContext()->UpdateSubresource( ObjectBuffer, 0, NULL, &XMMatrixTranspose( XMMatrixOrthographicLH( Device3D->GetWidth(), 
		                                             Device3D->GetHeight(), 0.1f, 1000.0f ) ), 0, 0 );

	Shader->SetConstantBufferVS( ObjectBuffer );

	Device3D->GetDeviceContext()->OMSetBlendState( Transparency, NULL, 0xffffffff );

	Device3D->GetDeviceContext()->Draw( 6 * Characters.size(), 0 );

	Characters.clear();
	
	return 0;
}

unsigned int Graphic::Text::GetNumCharacters()
{
	return Characters.size();
}

Graphic::Text::TextCharacter Graphic::Text::GetCharacter( unsigned int t )
{
   if( t >= Characters.size() )
   {
      return Graphic::Text::TextCharacter( ' ', 0, 0.0, 0.0, XMFLOAT4( 0.0, 0.0, 0.0, 1.0f ) );
   }
   else
   {
     return Graphic::Text::TextCharacter( Characters[t].symbol, Characters[t].size, Characters[t].x, Characters[t].y, Characters[t].color );
   }
}

////////Sprite////////

Graphic::Sprite::Sprite( Graphic::Device3D* Device, Graphic::Shader* shader  )
{
	Device3D = Device;
	Shader   = shader;
}

Graphic::Sprite::~Sprite()
{
	SAVE_RELEASE( VertexBuffer );
	SAVE_RELEASE( vertLayout );
	SAVE_RELEASE( Texture );
	SAVE_RELEASE( SamplerState );
	SAVE_RELEASE( Transparency );
	SAVE_RELEASE( ConstantBuffer );
}

HRESULT Graphic::Sprite::Create( char* name )
{
	Vertex v[] =
    {
		Vertex( -1.0f, -1.0f, 0.5f, 0.0f, 1.0f ),
		Vertex( -1.0f,  1.0f, 0.5f, 0.0f, 0.0f ),
		Vertex(  1.0f, -1.0f, 0.5f, 1.0f, 1.0f ),

		Vertex( -1.0f,  1.0f, 0.5f, 0.0f, 0.0f ),
		Vertex(  1.0f,  1.0f, 0.5f, 1.0f, 0.0f ),
		Vertex(  1.0f, -1.0f, 0.5f, 1.0f, 1.0f ),
    };

	D3D11_BUFFER_DESC vertexBufferDesc;
    ZeroMemory( &vertexBufferDesc, sizeof(vertexBufferDesc) );

    vertexBufferDesc.Usage          = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth      = sizeof( Vertex ) * 6;
    vertexBufferDesc.BindFlags      = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags      = 0;

    D3D11_SUBRESOURCE_DATA vertexBufferData;
    ZeroMemory( &vertexBufferData, sizeof(vertexBufferData) );

    vertexBufferData.pSysMem = v;

    V_RETURN( Device3D->GetDevice()->CreateBuffer( &vertexBufferDesc, &vertexBufferData, &VertexBuffer ) );

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
    UINT num = ARRAYSIZE( layout );

	Device3D->GetDevice()->CreateInputLayout( layout, num, Shader->GetVSBuffer()->GetBufferPointer(), Shader->GetVSBuffer()->GetBufferSize(), &vertLayout );

	// ImageInfo
	D3DX11_IMAGE_INFO      InfoFromFile;
	D3DX11_IMAGE_LOAD_INFO LoadImageInfo;

	ZeroMemory( &InfoFromFile, sizeof( InfoFromFile ) );
	ZeroMemory( &LoadImageInfo, sizeof( LoadImageInfo ) );

	V_RETURN( D3DX11GetImageInfoFromFile( name, NULL, &InfoFromFile, NULL ) );

	LoadImageInfo.Width    = InfoFromFile.Width;
	LoadImageInfo.Height   = InfoFromFile.Height;
	LoadImageInfo.Depth    = InfoFromFile.Depth;
	LoadImageInfo.Format   = InfoFromFile.Format;
	LoadImageInfo.pSrcInfo = &InfoFromFile;

	LoadImageInfo.FirstMipLevel  = 0;
	LoadImageInfo.MipLevels      = 1;
	LoadImageInfo.Usage          = D3D11_USAGE_DEFAULT;
	LoadImageInfo.BindFlags      = D3D11_BIND_SHADER_RESOURCE;
	LoadImageInfo.CpuAccessFlags = 0;
	LoadImageInfo.MiscFlags      = 0;
	LoadImageInfo.Filter         = D3DX11_FILTER_NONE;
	LoadImageInfo.MipFilter      = D3DX11_FILTER_NONE;

	V_RETURN( D3DX11CreateShaderResourceViewFromFile( Device3D->GetDevice(), name, &LoadImageInfo, NULL, &Texture, NULL ) );

	D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory( &sampDesc, sizeof(sampDesc) );

    sampDesc.Filter         = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD         = 0;
    sampDesc.MaxLOD         = D3D11_FLOAT32_MAX;

	V_RETURN( Device3D->GetDevice()->CreateSamplerState( &sampDesc, &SamplerState ) );

	D3D11_BUFFER_DESC cbbd;
    ZeroMemory( &cbbd, sizeof( D3D11_BUFFER_DESC ) ); 

    cbbd.Usage          = D3D11_USAGE_DEFAULT;
    cbbd.ByteWidth      = sizeof( sprite );
    cbbd.BindFlags      = D3D11_BIND_CONSTANT_BUFFER;
    cbbd.CPUAccessFlags = 0;
    cbbd.MiscFlags      = 0;

    V_RETURN( Device3D->GetDevice()->CreateBuffer( &cbbd, NULL, &ConstantBuffer ) );

	// BlenderState
	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
    ZeroMemory( &rtbd, sizeof(rtbd) );

    rtbd.BlendEnable           = true;
    rtbd.BlendOp               = D3D11_BLEND_OP_ADD;
    rtbd.SrcBlend              = D3D11_BLEND_SRC_ALPHA;
    rtbd.DestBlend             = D3D11_BLEND_INV_SRC_ALPHA;
    rtbd.DestBlendAlpha        = D3D11_BLEND_ONE;
    rtbd.SrcBlendAlpha         = D3D11_BLEND_ONE;
    rtbd.BlendOpAlpha          = D3D11_BLEND_OP_ADD;
    rtbd.RenderTargetWriteMask = D3D10_COLOR_WRITE_ENABLE_ALL;

    D3D11_BLEND_DESC blendDesc;
    ZeroMemory( &blendDesc, sizeof(blendDesc) );

    blendDesc.AlphaToCoverageEnable = false;
    blendDesc.RenderTarget[0] = rtbd;

    Device3D->GetDevice()->CreateBlendState( &blendDesc, &Transparency );
}

HRESULT Graphic::Sprite::Draw( float x, float y, float scalX, float scalY, float transparence, float rotateZ )
{
	XMMATRIX Scal  = XMMatrixScaling( scalX, scalY, 1 );
	XMMATRIX Rot   = XMMatrixRotationZ( XMConvertToRadians( rotateZ ) );
	XMMATRIX Trans = XMMatrixTranslation( x, y, 0 );

	XMMATRIX World = XMMatrixIdentity();
	World = Rot * Trans * Scal;
	
	sprite sp;
	ZeroMemory( &sp, sizeof( sp ) );

	sp.World          = World;
	sp.Transparence.w = transparence;
	
	Device3D->GetDeviceContext()->UpdateSubresource( ConstantBuffer, 0, NULL, &sp, 0, 0 );

	Shader->SetConstantBufferVS( ConstantBuffer );
	Shader->SetConstantBufferPS( ConstantBuffer );

	Shader->SetResourceViewAndSamplerState( Texture, SamplerState );
	
	UINT stride = sizeof( Vertex );
    UINT offset = 0;
	
	Device3D->GetDeviceContext()->IASetVertexBuffers( 0, 1, &VertexBuffer, &stride, &offset );

	Device3D->GetDeviceContext()->IASetInputLayout( vertLayout );
    Device3D->GetDeviceContext()->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	Device3D->GetDeviceContext()->OMSetBlendState( Transparency, NULL, 0xffffffff );

	Shader->Apply();

    Device3D->GetDeviceContext()->Draw( 6, 0 );

	return S_OK;
}

////////Mesh////////

Graphic::Mesh::Mesh( Graphic::Device3D* Device, Graphic::Shader* Sh, Graphic::Camera* Cam )
{
	Device3D = Device;
	Shader = Sh;
	Camera = Cam;
	
	pVertexLayout = NULL;
	pVertexBuffer = NULL;
	pIndexBuffer  = NULL;

	Texture = NULL;
}

Graphic::Mesh::~Mesh()
{	
	SAVE_RELEASE( pVertexLayout );
	SAVE_RELEASE( pVertexBuffer );
	SAVE_RELEASE( pIndexBuffer );
	SAVE_RELEASE( Texture );
	SAVE_RELEASE( SamplerState )
	SAVE_RELEASE( ConstantBuffer );

	delete vertices;
	delete indices;
}

HRESULT Graphic::Mesh::Create( char* file, char* tex )
{
	V_RETURN( D3DX11CreateShaderResourceViewFromFile( Device3D->GetDevice(), tex, NULL, NULL, &Texture, NULL ) );
	
	LoadMeshFromFile( file );

	
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },   
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D10_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
    UINT num = ARRAYSIZE( layout );

	Device3D->GetDevice()->CreateInputLayout( layout, num, Shader->GetVSBuffer()->GetBufferPointer(), Shader->GetVSBuffer()->GetBufferSize(), &pVertexLayout );

	vertices = NULL;
	indices  = NULL;
	
	vertices = new Vertex[buffer.vertices.size()];
	indices  = new UINT[buffer.indices.size()];

	for( int i=0; i<buffer.vertices.size(); i++ )
		vertices[i] = buffer.vertices[i];

	for( int i=0; i<buffer.indices.size(); i++ )
		indices[i] = buffer.indices[i];

	verticesCount = buffer.vertices.size();
	indicesCount  = buffer.indices.size();

	buffer.vertices.clear();
	buffer.indices.clear();
	
	D3D11_BUFFER_DESC bd;
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof( Vertex ) * (verticesCount + 1);
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bd.CPUAccessFlags = 0;
    bd.MiscFlags = 0;

    D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = vertices;

    V_RETURN( Device3D->GetDevice()->CreateBuffer( &bd, &InitData, &pVertexBuffer ) );

	bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof( UINT ) * indicesCount;        
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    bd.CPUAccessFlags = 0;
    bd.MiscFlags = 0;

	InitData.pSysMem = indices;

    V_RETURN( Device3D->GetDevice()->CreateBuffer( &bd, &InitData, &pIndexBuffer ) );

	D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory( &sampDesc, sizeof(sampDesc) );

    sampDesc.Filter         = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD         = 0;
    sampDesc.MaxLOD         = D3D11_FLOAT32_MAX;

	V_RETURN( Device3D->GetDevice()->CreateSamplerState( &sampDesc, &SamplerState ) );

	D3D11_BUFFER_DESC cbbd;
    ZeroMemory( &cbbd, sizeof( D3D11_BUFFER_DESC ) ); 

    cbbd.Usage          = D3D11_USAGE_DEFAULT;
    cbbd.ByteWidth      = sizeof( mesh );
    cbbd.BindFlags      = D3D11_BIND_CONSTANT_BUFFER;
    cbbd.CPUAccessFlags = 0;
    cbbd.MiscFlags      = 0;

    V_RETURN( Device3D->GetDevice()->CreateBuffer( &cbbd, NULL, &ConstantBuffer ) );

	return S_OK;
}

void Graphic::Mesh::Draw( float x, float y, float z, float rotX, float rotY, float rotZ, float scal )
{
	XMMATRIX Scal  = XMMatrixScaling( scal, scal, scal );
	XMMATRIX Rot   = XMMatrixRotationRollPitchYaw( XMConvertToRadians( rotX ), XMConvertToRadians( rotY ), XMConvertToRadians( rotZ ) );
	XMMATRIX Trans = XMMatrixTranslation( x, y, z );

	XMMATRIX World = XMMatrixIdentity();
	World = Scal * Rot * Trans;
	
	mesh mh;
	ZeroMemory( &mh, sizeof( mh ) );

	mh.WVP = World * Camera->GetView() * Camera->GetProjection();
	
	Device3D->GetDeviceContext()->UpdateSubresource( ConstantBuffer, 0, NULL, &mh, 0, 0 );

	Shader->SetConstantBufferVS( ConstantBuffer );

	Shader->SetResourceViewAndSamplerState( Texture, SamplerState );
	
	UINT stride = sizeof( Vertex );
    UINT offset = 0;
	
	Device3D->GetDeviceContext()->IASetVertexBuffers( 0, 1, &pVertexBuffer, &stride, &offset );

	Device3D->GetDeviceContext()->IASetIndexBuffer( pIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );

	Device3D->GetDeviceContext()->IASetInputLayout( pVertexLayout );

	Device3D->GetDeviceContext()->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	Shader->Apply();

	Device3D->GetDeviceContext()->DrawIndexed( indicesCount, 0, 0 ); 
}

void Graphic::Mesh::LoadMeshFromFile( char* file )
{
	buffer.indices.clear();
	buffer.vertices.clear();

	vector<XMFLOAT3> Positions;
    vector<XMFLOAT2> TexCoords;
    vector<XMFLOAT3> Normals;

	string Command;
    ifstream cin( file );

	if( !cin ) 
		return;

	while( !cin.eof() )
	{
		cin >> Command;

		if( Command == "#" )
		{
			getline( cin, Command );

			continue;
		}
		
		if( Command == "v" )
		{
			float x, y, z;
            cin >> x >> y >> z;

            Positions.insert( Positions.end(), XMFLOAT3( x, y, z ) );

			continue;
		}
		
		if( Command == "vt" )
		{
			float u, v;
            cin >> u >> v;

			TexCoords.insert( TexCoords.end(), XMFLOAT2( u, -v ) );

			continue;
		}
		
		if( Command == "vn" )
		{
			float x, y, z;
            cin >> x >> y >> z;

            Normals.insert( Normals.end(), XMFLOAT3( x, y, z ) );
			
			continue;
		}
		
		if( Command == "f" )
		{
			UINT iPosition, iTexCoord, iNormal;
            Vertex vertex;

			for( UINT iFace = 0; iFace < 3; iFace++ )
            {
                ZeroMemory( &vertex, sizeof( Vertex ) );

                cin >> iPosition;
                vertex.position = Positions[ iPosition - 1 ];

				char buf;
				cin >> buf;

                cin >> iTexCoord;
                vertex.texcoord = TexCoords[ iTexCoord - 1 ];

                cin >> buf;

                cin >> iNormal;
                vertex.normal = Normals[ iNormal - 1 ];

                int index = AddVertex( iPosition, vertex );
                buffer.indices.insert( buffer.indices.end(), index );
            }
			
			continue;
		}
	}

	cin.close();

	Positions.clear();
    TexCoords.clear();
    Normals.clear();
}

int Graphic::Mesh::AddVertex(int vertexI, Vertex vertex)
{	
	for( int i=0; i<buffer.vertices.size(); i++ )
		if ( memcmp( &buffer.vertices[i], &vertex, sizeof( Vertex ) ) == 0 ) 
			return i;
	
	buffer.vertices.insert( buffer.vertices.end(), vertex ); 

	return buffer.vertices.size() - 1; 
}