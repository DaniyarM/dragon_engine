Texture2D    ObjTexture;
SamplerState ObjSamplerState;

cbuffer ConstantBuffer
{
   float4x4 matOrtho;
};

struct VS_INPUT
{
	float4 Pos      : POSITION;
	float2 TexCoord : TEXCOORD;
	float4 Color    : COLOR;
};

struct PS_INPUT
{
   float4 Pos      : SV_POSITION;
   float2 TexCoord : TEXCOORD;
   float4 Color    : COLOR;
};

PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output;

    output.Pos      = mul( input.Pos, matOrtho );
    output.TexCoord = input.TexCoord;
    output.Color    = input.Color;

    return output;
}

float4 PS( PS_INPUT input ) : SV_TARGET
{
   float4 result = ObjTexture.Sample( ObjSamplerState, input.TexCoord );

   result[0] = input.Color[0];
   result[1] = input.Color[1];
   result[2] = input.Color[2];

   return result;
}