Texture2D    ObjTexture;
SamplerState ObjSamplerState;

cbuffer ConstantBuffer
{
    float4x4 World;
	float4   Transparence;
};

struct VS_INPUT
{
	float4 Pos      : POSITION;
	float2 TexCoord : TEXCOORD;
};

struct PS_INPUT
{
    float4 Pos      : SV_POSITION;
    float2 TexCoord : TEXCOORD;
};

PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output;

	input.Pos = mul( input.Pos, World );

    output.Pos      = input.Pos;
    output.TexCoord = input.TexCoord;

    return output;
}

float4 PS( PS_INPUT input ) : SV_TARGET
{
    float4 result = ObjTexture.Sample( ObjSamplerState, input.TexCoord );

	result[3] = Transparence[3];
	
	return result;
}