/**************************************************
Dragon Engine 0.1

Graphic_Kernel (.h)

Copyright: Mazitov Daniyar (cyberdan)
**************************************************/
#ifndef GRAPHIC_KERNEL_H
#define GRAPHIC_KERNEL_H

//-------------------------------------------------
// Debug settings
//-------------------------------------------------
#define SHOW_ERRORS 1
//-------------------------------------------------


//-------------------------------------------------
// Warnings disable
//-------------------------------------------------
#pragma warning ( disable : 4005 )
#pragma warning ( disable : 4018 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4305 )
#pragma warning ( disable : 4715 )
#pragma warning ( disable : 4996 )
#pragma warning ( disable : 4800 )
//#define _CRT_SECURE_NO_WARNINGS
//-------------------------------------------------


//-------------------------------------------------
// Includes
//-------------------------------------------------
#include <D3DX11.h>
#include <D3D11.h>
#include <xnamath.h> //#include <D3DX10math.h>
//-------------------------------------------------
#include <DXGI.h>
//#include <DWrite.h>
//#include <D2D1.h>
//-------------------------------------------------
#include <vector>
#include <string>
#include <fstream>
//-------------------------------------------------
#include <Windows.h>
//-------------------------------------------------


//-------------------------------------------------
// Namespaces
//-------------------------------------------------
using namespace std;
//-------------------------------------------------


//-------------------------------------------------
// Include mandatory kernels 
//-------------------------------------------------
#include "System_Kernel.h"
//-------------------------------------------------


//-------------------------------------------------
// Macroses
//-------------------------------------------------
#define MsgBox( tit, mes ) MessageBox( NULL, mes, tit, MB_OK );
//-------------------------------------------------
#define V_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return E_FAIL;                               \
	}                                                \
}
//-------------------------------------------------
#define C_RETURN( x )                                \
{                                                    \
	if( FAILED( x ) )                                \
	{                                                \
		if( SHOW_ERRORS )                            \
		    MsgBox( "������!", #x );                 \
			                                         \
		return 1;                                    \
	}                                                \
}
//-------------------------------------------------
#define SAVE_RELEASE( x )                            \
{                                                    \
	if( x )                                          \
		x->Release();                                \
}                                          
//-------------------------------------------------


//-------------------------------------------------
// Typedefes
//-------------------------------------------------
typedef bool (CALLBACK*Func)();
//-------------------------------------------------


namespace Graphic
{
	class Device3D
	{
	public:
		Device3D( mSystem::Application* App );
		~Device3D();

		void SetCreateDeviceFunction( Func CreateDevice );
		void SetDestroyDeviceFunction( Func DestroyDevice );
		void SetFrameMoveFunction( Func FrameMove );
		void SetFrameRenderFunction( Func FrameRender );
		void SetResizeSwapChainFunction( Func ResizeSwapChain );

		HRESULT Create( bool windowed );
		HRESULT FullScreen( bool windowed );

		float GetTime();

		D3D_DRIVER_TYPE   GetDriverType();
		D3D_FEATURE_LEVEL GetFeatureLevel();

		UINT GetWidth();
		UINT GetHeight();

		IDXGISwapChain*         GetSwapChain();
		ID3D11Device*           GetDevice();
		ID3D11DeviceContext*    GetDeviceContext();
		ID3D11RenderTargetView* GetRenderTargetView();
		ID3D11DepthStencilView* GetDepthStencilView();

		IDXGIDevice*            GetDXGIDevice();
		IDXGIAdapter*           GetDXGIAdapter();
		IDXGIFactory*           GetDXGIFactory();

		void ClearAll( float* color = NULL );
		void ClearRTV( float* color = NULL );
		void ClearDSV();

		void Present();

		static void Frame( void* Obj );

	protected:
		HWND hWnd;

		mSystem::Application* Application;

		bool Fullscreen;

		float width;
		float height;

		D3D_DRIVER_TYPE          DriverType;
		D3D_FEATURE_LEVEL        FeatureLevel;

		IDXGISwapChain*         pSwapChain;
		ID3D11Device*           pDevice;
		ID3D11DeviceContext*    pDeviceContext;
		ID3D11RenderTargetView* pRenderTargetView;
		ID3D11DepthStencilView* pDepthStencilView;

		IDXGIDevice *  pDXGIDevice;
		IDXGIAdapter * pDXGIAdapter;
		IDXGIFactory * pDXGIFactory;

		HRESULT Frame();

		Func OnCreateDevice;
		Func OnDestroyDevice;
		Func OnFrameMove;
		Func OnFrameRender;
		Func OnResizeSwapChain;
	};

	class Shader
	{
	public:
		Shader( Device3D* Device );
		~Shader();

		HRESULT Create( char* file, char* version = "4_0", char* VertexShaderName = "VS", char* PixelShaderName = "PS", char* HullShaderName = NULL,
			            char* DomainShaderName = NULL, char* GeometryShaderName = NULL, char* ComputeShaderName = NULL );

		void SetConstantBufferVS( ID3D11Buffer* ObjectBuffer );
		void SetConstantBufferPS( ID3D11Buffer* ObjectBuffer );
		void SetConstantBufferHS( ID3D11Buffer* ObjectBuffer );
		void SetConstantBufferDS( ID3D11Buffer* ObjectBuffer );
		void SetConstantBufferGS( ID3D11Buffer* ObjectBuffer );
		void SetConstantBufferCS( ID3D11Buffer* ObjectBuffer );

		void SetResourceViewAndSamplerState( ID3D11ShaderResourceView* constant, ID3D11SamplerState* SamplerState );

		ID3D10Blob* GetVSBuffer();

		void Apply();

	protected:
		UINT   VSBufferSize;
		LPVOID VSBufferPointer;

		ID3D10Blob* VS_Buffer;
		ID3D10Blob* PS_Buffer;
		ID3D10Blob* HS_Buffer;
		ID3D10Blob* DS_Buffer;
		ID3D10Blob* GS_Buffer;
		ID3D10Blob* CS_Buffer;

		ID3D11VertexShader* VS;
		ID3D11PixelShader*  PS;

		ID3D11HullShader*   HS;
		ID3D11DomainShader* DS;

		ID3D11GeometryShader* GS;

		ID3D11ComputeShader* CS;

		//ID3D11RasterizerState* RsasterizerState;

		Device3D* Device3D;
	};

	class Camera
	{
	public:
		Camera( Device3D* Device );
		~Camera();

		void UpdatePerspective();

		XMMATRIX GetView();
		XMMATRIX GetProjection();

		void TranslationX( float trans );
		void TranslationY( float trans );
		void TranslationZ( float trans );

		void RotateX( float trans );
		void RotateY( float trans );
		void RotateZ( float trans );
	
	protected:
		struct FLOAT3
		{
			FLOAT3()
			{
				x = y = z = 0;
			}

			float x;
			float y;
			float z;
		};

		XMMATRIX mView;
		XMMATRIX mProjection;

		FLOAT3 Eye;
		FLOAT3 LookAt;
		FLOAT3 Up;

		float RotationX;
		float RotationY;

		Device3D* Device3D;
	};

	class Text
	{
	public:
		Text( Device3D* Device, Shader* Shader );
		~Text();

		HRESULT Create( char* name );
		bool    Draw( string text, UINT size, XMFLOAT4 color, UINT left, UINT top );

	protected:
		struct TextCharacter
		{
			TextCharacter(){};
			TextCharacter( char Symbol, int Size, float X, float Y, XMFLOAT4 Color )
			{
				symbol = Symbol;
				size = Size;
				x = X;
				y = Y;
				color = Color;
			}

			float x, y;
			XMFLOAT4 color;
			char symbol;
			int size;
		};

		vector<TextCharacter> Characters;

		struct TextVertex
		{
			TextVertex(){}
			TextVertex( float x, float y, float z, float u, float v, XMFLOAT4 xColor ) : pos( x, y, z ), texCoord( u, v ), Color( xColor ){}

			XMFLOAT3 pos;
			XMFLOAT2 texCoord;
			XMFLOAT4 Color;
		};

		TextVertex* vertices;

		unsigned int GetNumCharacters();

		TextCharacter GetCharacter( unsigned int t );
    
		ID3D11InputLayout* vertLayout;
		ID3D11Buffer*      VertexBuffer;

		ID3D11ShaderResourceView* Texture;
		ID3D11SamplerState*       TexSamplerState;
		ID3D11Buffer*             ObjectBuffer;

		ID3D11BlendState* Transparency;

		Shader*   Shader;
		Device3D* Device3D;
	};

	class Sprite
	{
	public:
		Sprite( Graphic::Device3D* Device, Graphic::Shader* shader  );
		~Sprite();

		HRESULT Create( char* name );
		HRESULT Draw( float x, float y, float scalX=1, float scalY=1, float transparence=1, float rotateZ = 0 );

	private:
		struct Vertex
		{
			Vertex(){}
			Vertex( float x, float y, float z, float u, float v ) : pos( x, y, z ), texCoord( u, v ){}

			XMFLOAT3 pos;
			XMFLOAT2 texCoord;
		};

		struct sprite
		{
			XMMATRIX World;;
			XMFLOAT4 Transparence;
		};

		ID3D11Buffer*      VertexBuffer;
		ID3D11InputLayout* vertLayout;

		ID3D11ShaderResourceView* Texture;
		ID3D11SamplerState*       SamplerState;

		ID3D11BlendState* Transparency;

		ID3D11Buffer* ConstantBuffer;

		Device3D* Device3D;
		Shader*   Shader;
	};

	class Mesh
	{
	public:
		Mesh( Device3D* Device, Shader* Sh, Camera* Cam );
		~Mesh();

		HRESULT Create( char* file, char* tex );
		void    Draw( float x = 0, float y = 0, float z = 0, float rotX = 0, float rotY = 0, float rotZ = 0, float scal = 1 );

	protected:
		ID3D11InputLayout* pVertexLayout;
		ID3D11Buffer*      pVertexBuffer;
		ID3D11Buffer*      pIndexBuffer;

		ID3D11ShaderResourceView* Texture;
		ID3D11SamplerState*       SamplerState;

		struct Vertex
		{
			Vertex(){}
			Vertex( float x, float y, float z, float nx, float ny, float nz, float u, float v ) : position( x, y, z ), normal( nx, ny, nz ), texcoord( u, v )
			{}
			
			XMFLOAT3 position;
			XMFLOAT3 normal;
			XMFLOAT2 texcoord;
		};

		struct mesh
		{
			XMMATRIX WVP;
		};

		struct 
		{
			vector<Vertex> vertices;
			vector<UINT>  indices;
		} buffer;

		Vertex* vertices;
		UINT*  indices;

		UINT verticesCount;
	    UINT indicesCount;

		void LoadMeshFromFile( char* file );
		int AddVertex( int vertexI, Vertex vertex );

		ID3D11Buffer* ConstantBuffer;

		Device3D* Device3D;
		Shader*   Shader;
		Camera*  Camera;
	};
};

#endif